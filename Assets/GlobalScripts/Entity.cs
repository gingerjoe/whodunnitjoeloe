﻿using UnityEngine;
using System.Collections;
using StateManagement;

public class Entity : MonoBehaviour
{
    private StateMachine<EntityState, EntityTrigger> _entityStateMachine;

    private enum EntityState
    {
        Idle,
        Walking,
        Talking
    }

    public enum EntityTrigger
    {
        StartCoversation,
        WalkToLocation,
        EndConversation,
        ArriveAtLocation
    }

    void Start()
    {
        _entityStateMachine = new StateMachine<EntityState, EntityTrigger>(EntityState.Idle);

        _entityStateMachine.Configure(EntityState.Idle)
            .Permit(EntityTrigger.WalkToLocation, EntityState.Walking)
            .Permit(EntityTrigger.StartCoversation, EntityState.Talking);

        _entityStateMachine.Configure(EntityState.Walking)
            .Permit(EntityTrigger.ArriveAtLocation, EntityState.Idle); 

        _entityStateMachine.Configure(EntityState.Talking)
            .Permit(EntityTrigger.EndConversation, EntityState.Idle);
    }

}