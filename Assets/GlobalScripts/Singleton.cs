﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;
    private static bool _applicationIsQuitting = false;
    private static readonly object _handle = new object();

    public static T Instance
    {
        get
        {
            if (_applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Application exiting, instance destroyed. Returning null");
                return null;
            }

            lock (_handle)
            {
                if (_instance != null) return _instance;

                _instance = (T) FindObjectOfType(typeof(T));
                if (FindObjectsOfType(typeof(T)).Length > 1)
                {
                    Debug.LogError("[Singleton] Fatal error: More than 1 Singleton exists");
                    return _instance;
                }

                if (_instance == null)
                {
                    GameObject singleton = new GameObject();
                    _instance = singleton.AddComponent<T>();
                    singleton.name = "(singleton) " + typeof(T).ToString();

                    DontDestroyOnLoad(singleton);

                    Debug.Log("[Singleton] An instance of " + typeof(T) + " created");
                }
                else
                {
                    Debug.Log("[Singleton] Instance already exists: " + _instance.gameObject.name);
                }

                return _instance;
            }
        }
    }

    void Awake()
    {
        if (_instance && _instance != this)
        {
            DestroyImmediate(gameObject);
        }
    }

    void OnDestroy()
    {
        _applicationIsQuitting = true;
    }
}