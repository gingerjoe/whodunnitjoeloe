﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveManager : Singleton<SaveManager> {

    private Dictionary<string, string> _inkVariablesDictionary;
    private Dictionary<string, List<string>> _objectivesDictionary;
    private Dictionary<string, List<Pair<string>>> _timesDictionary;
    private Dictionary<string, bool> _boolDictionary;

    void update()
    {
        Debug.Log("Im here!!!!");
    }

    public Dictionary<string, string> InkVariables
    {
        get
        {
            return _inkVariablesDictionary;
        }
    }

    public Dictionary<string, List<string>> Objectives
    {
        get
        {
            return _objectivesDictionary;
        }
    }

    public Dictionary<string, List<Pair<string>>> Times
    {
        get
        {
            return _timesDictionary;
        }
    }

    public void SetInkVarsAndObjectivesAndTimes(Dictionary<string,string> variables, Dictionary<string, List<string>> objectives, Dictionary<string, List<Pair<string>>> times)
    {
        _inkVariablesDictionary = variables;
        _objectivesDictionary = objectives;
        _timesDictionary = times;
    }

    public bool HasBool(string boolKey)
    {
        return _boolDictionary.ContainsKey(boolKey);
    }

    public bool GetBool(string boolKey)
    {
        if (_boolDictionary.ContainsKey(boolKey))
        {
            return _boolDictionary[boolKey];
        }
        else
        {
            Debug.LogError(string.Concat("Bool: '", boolKey, "' does not exist"));
            throw new Exception("ERROR --- i dont know a good error message to put here");
        }
    }

    public void SetBool(string boolKey, bool value)
    {
        if (_boolDictionary.ContainsKey(boolKey))
        {
            bool dictValue = _boolDictionary[boolKey];
            Debug.LogWarning(string.Concat("Bool already exists in current context, changing from:", dictValue, ", To:", value));
        }
        _boolDictionary[boolKey] = value;
    }

    public void RemoveBool(string boolKey)
    {
        if (_boolDictionary.ContainsKey(boolKey))
        {
            bool dictValue = _boolDictionary[boolKey];
            Debug.LogWarning(string.Concat("Removing:{", boolKey, ":", dictValue, "} from SaveManager"));
            _boolDictionary.Remove(boolKey);
        }
        else
        {
            Debug.LogError(string.Concat("Tried to remove:", boolKey, ", from SaveManager, but Bool does not exist already"));
        }
    }

    public void SaveToFile()
    {
        Debug.Log("Saving...");

        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Create(Global.Instance.GetSavePath());

        try
        {
            CheckDictionarysAreNotNull();

            SaveData data = new SaveData();
            data.inkVariablesDictionary = _inkVariablesDictionary;
            data.objectivesDictionary = _objectivesDictionary;
            data.timesDictionary = _timesDictionary;
            data.boolDictionary = _boolDictionary;

            bf.Serialize(fs, data);
            Debug.Log("Save Successfull");
        }
        catch(Exception e)
        {
            Debug.LogError("Save failed, " + e.Message);
        }
        
        fs.Close();
    }

    public bool LoadFromFile()
    {
        bool success = false;

        string path = Global.Instance.GetSavePath();
        Debug.Log("Loading from file path: " + path);
        if (File.Exists(path))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.Open(path, FileMode.Open);

            try
            {
                SaveData data = (SaveData)bf.Deserialize(fs);
                SetInkVarsAndObjectivesAndTimes(data.inkVariablesDictionary, data.objectivesDictionary, data.timesDictionary);
                _boolDictionary = data.boolDictionary;
                success = true;
            }
            catch (Exception e)
            {
                Debug.LogError(string.Concat("Unable to Deserialize save file: ", e.Message));
                
                InitDefaultDictionarys();
            }

            fs.Close();
        }
        else
        {
            Debug.LogError("Unable to load from file, file does not exist. [" + path + "] ");

            InitDefaultDictionarys();
        }

        Debug.Log(string.Concat("Loaded successfully = ", success));
        return success;
    }

    private void CheckDictionarysAreNotNull()
    {
        if (_inkVariablesDictionary == null)
        {
            Debug.LogError("inkVariablesDictionary is null, creating empty");
            _inkVariablesDictionary = new Dictionary<string, string>();
        }
        if(_objectivesDictionary == null)
        {
            Debug.LogError("objectivesDictionary is null, creating empty");
            _objectivesDictionary = new Dictionary<string, List<string>>();
        }
        if(_timesDictionary == null)
        {
            Debug.LogError("timesDictionary is null, creating empty");
            _timesDictionary = new Dictionary<string, List<Pair<string>>>();
        }
        if(_boolDictionary == null)
        {
            Debug.LogError("boolDictionary is null, creating empty");
            _boolDictionary = new Dictionary<string, bool>();
        }
    }

    private void InitDefaultDictionarys()
    {
        Debug.LogWarning("Initialising empty dictionarys");

        _inkVariablesDictionary = new Dictionary<string, string>();
        _objectivesDictionary = new Dictionary<string, List<string>>();
        _timesDictionary = new Dictionary<string, List<Pair<string>>>();
        _boolDictionary = new Dictionary<string, bool>();
    }

    public void ResetSave(bool hardReset=true)
    {
        InitDefaultDictionarys();
        if (hardReset)
        {
            SaveToFile();
        }
    }
}

[Serializable]
class SaveData
{
    public Dictionary<string, string> inkVariablesDictionary;
    public Dictionary<string, List<string>> objectivesDictionary;
    public Dictionary<string, List<Pair<string>>> timesDictionary;
    public Dictionary<string, bool> boolDictionary;
}
