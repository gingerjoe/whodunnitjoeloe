﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Pair<T> {
    public Pair() {
    }

    public Pair(T first, T second) {
        this.First = first;
        this.Second = second;
    }

    public T First { get; set; }
    public T Second { get; set; }
};

