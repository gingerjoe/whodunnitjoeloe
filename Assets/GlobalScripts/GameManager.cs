﻿using UnityEngine;
using System.Collections;
using StateManagement;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    StateMachine<MenuState, MenuTrigger> _menuStateManager;
    StateMachine<GameplayState, GameplayTrigger> _gameplayStateManager;

    private enum MenuState
    {
        MainMenu,
        Paused,
        Exploring,
        Interacting
    }

    public enum MenuTrigger
    {
        StartGame,
        Pause,
        Resume,
        Interact,
        StopInteracting
    }

    private enum GameplayState
    {
        SurfaceScanActive,
        Exploring,
        InteractingNpc,
        InteractingItem
    }

    public enum GameplayTrigger
    {
        SurfaceScanComplete,
        InteractItem,
        InteractNpc,
        HoverItem,
        ExitState
    }

    void Start()
    {
        InitMenuStateManager();
        InitGameplayStateManager();
    }

    private void InitGameplayStateManager()
    {
        //_gameplayStateManager = new StateMachine<GameplayState, GameplayTrigger>(GameplayState.Exploring);
        //if (SaveManager.Instance.GetBool(SceneManager.GetActiveScene().name + "_scanned"))
        //{
        _gameplayStateManager = new StateMachine<GameplayState, GameplayTrigger>(GameplayState.Exploring);
        //}
        //else
        //{
        //    _gameplayStateManager = new StateMachine<GameplayState, GameplayTrigger>(GameplayState.SurfaceScanActive);
        //}

        _gameplayStateManager.Configure(GameplayState.SurfaceScanActive)
            .OnExit((t) => SurfaceScanComplete())
            .Permit(GameplayTrigger.SurfaceScanComplete, GameplayState.Exploring);

        _gameplayStateManager.Configure(GameplayState.Exploring)
            //.OnEnter((t) => InitialiseHiddenItems())
            .Permit(GameplayTrigger.InteractItem, GameplayState.InteractingItem)
            .Permit(GameplayTrigger.InteractNpc, GameplayState.InteractingNpc);

        _gameplayStateManager.Configure(GameplayState.InteractingItem)
            .Permit(GameplayTrigger.ExitState, GameplayState.Exploring);

        _gameplayStateManager.Configure(GameplayState.InteractingNpc)
            .Permit(GameplayTrigger.ExitState, GameplayState.Exploring);
    }

    private void SurfaceScanComplete()
    {
        //Remove surface scan elements from scene
        //Save surface scan state for scene
    }

    private void InitMenuStateManager()
    {
        _menuStateManager = new StateMachine<MenuState, MenuTrigger>(MenuState.MainMenu);

        _menuStateManager.Configure(MenuState.MainMenu)
            .Permit(MenuTrigger.StartGame, MenuState.Exploring);

        _menuStateManager.Configure(MenuState.Paused)
            .Permit(MenuTrigger.Resume, MenuState.Exploring);

        _menuStateManager.Configure(MenuState.Exploring)
            .Permit(MenuTrigger.Interact, MenuState.Interacting)
            .Permit(MenuTrigger.Pause, MenuState.Paused);

        _menuStateManager.Configure(MenuState.Interacting)
            .Permit(MenuTrigger.StopInteracting, MenuState.Exploring);

        _menuStateManager.Fire(MenuTrigger.StartGame); //TODO Update when we have a menu
    }

    public void InteractItem(GameObject obj)
    {
        InteractiveItem item = obj.GetComponent<InteractiveItem>();
        _gameplayStateManager.Fire(GameplayTrigger.InteractItem);
        GameplayState state = _gameplayStateManager.CurrentState;
        if (state == GameplayState.InteractingItem)
        {
            item.Fire(InteractiveItem.ItemTrigger.Interact);
        }
    }

    public void InteractNpc(GameObject obj)
    {
        //ConversationManager conversationManager = GameObject.Find("ConversationManager").GetComponent<ConversationManager>();
        _gameplayStateManager.Fire(GameplayTrigger.InteractNpc);
        GameplayState state = _gameplayStateManager.CurrentState;
        if (state == GameplayState.InteractingNpc)
        {
            ConversationManager.Instance.BeginConversation(obj);
        }
    }

    public void Fire(MenuTrigger trigger)
    {
        _menuStateManager.Fire(trigger);
    }

    public void Fire(GameplayTrigger trigger)
    {
        _gameplayStateManager.Fire(trigger);
    }
}
