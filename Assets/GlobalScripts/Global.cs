﻿using UnityEngine;
using System.Collections;

public class Global : Singleton<Global>
{
    protected Global()
    {
    }

    void Awake()
    {
        PlayerSaveFilePath = Application.persistentDataPath + PlayerSaveFilePath;
        Debug.Log(string.Concat("Save File Path = ", PlayerSaveFilePath));
    }

    //Inspector variables
    public Color HighlightColor = Color.yellow;
    public Color FocusScanBorderColor = Color.green;

    public float PanelSlideAnimationTime = 1.0f;
    public float FocusScanBorderAnimationTime = 1.0f;
    public float FocusScanBorderThickness = 0.4f;

    //Static variables
    public static float PlayerHeight = 1.0f;
    public static string PlayerName = "Padma";
    public static string PlayerPrefix = "P";
    public float FocusScanBorderDrawSpeed = 100f;

    public string PlayerSaveFilePath = "/playerSave.dat";

    public string GetSavePath()
    {
        return PlayerSaveFilePath;
    }

    public string GetPlayerName()
    {
        return PlayerName;
    }

    public string GetPlayerPrefix()
    {
        return PlayerPrefix;
    }
}