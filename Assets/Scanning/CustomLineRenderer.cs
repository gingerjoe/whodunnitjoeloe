﻿using UnityEngine;
using System.Collections;

public class CustomLineRenderer {

    private GameObject _parent;
    private LineRenderer _currentLineRenderer;
    private Vector3 _latestPoint;
    private float _borderThickness;
    private Material focusBorderMaterial;

    public CustomLineRenderer(GameObject parent, Vector3 startPoint, float thickness){

       focusBorderMaterial = (Material)Resources.Load("Materials/FocusBorder", typeof(Material));

        //TODO add children to parents
        _parent = parent;
        _borderThickness = thickness;

        _currentLineRenderer = AddNewLineRenderer(parent);
        _currentLineRenderer.SetPosition(0, startPoint);
        _currentLineRenderer.SetPosition(1, startPoint);

        _latestPoint = startPoint;
    }

    private LineRenderer AddNewLineRenderer(GameObject parent){
        GameObject child = new GameObject();
        child.transform.parent = parent.transform;
        LineRenderer lineRenderer = child.AddComponent<LineRenderer>() as LineRenderer;
        lineRenderer.useWorldSpace = false;
        lineRenderer.SetWidth(_borderThickness, _borderThickness);
        lineRenderer.SetVertexCount(2);

        lineRenderer.material = focusBorderMaterial;
        return lineRenderer;
    }

    public void AddLine(){
        _currentLineRenderer = AddNewLineRenderer(_parent);
        _currentLineRenderer.SetPosition(0, _latestPoint);
        _currentLineRenderer.SetPosition(1, _latestPoint);
    }

    public void UpdatePoint(Vector3 updatedPoint){
        _currentLineRenderer.SetPosition(1, updatedPoint);
        _latestPoint = updatedPoint;
    }

    public void SetColor(Color color)
    {
        focusBorderMaterial.color = color;
    }
}
