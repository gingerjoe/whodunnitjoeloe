﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SurfaceScan : MonoBehaviour {

    public Button ScanButton;
    public RectTransform pixelPanel;
    public float scanSpeed = 4.0f;
    public float panSpeed = 0.2f;

    private bool _depixel;
    private GameObject[] _items;
    private CameraPanning _cam;
    private float _boundsX;

    void Start ()
    {
        //if not previously scanned
        _cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraPanning>();
        ScanButton = gameObject.GetComponentInChildren<Button>();
        pixelPanel = GameObject.Find("SurfaceScanUI/Panel").GetComponent<RectTransform>();
        _depixel = false;
        _items = GameObject.FindGameObjectsWithTag("Item");
        _boundsX = Screen.width + pixelPanel.rect.xMax + 20;
        //else statemachine skips surface scan, disable self, do not proceed with any of this script
    }

    void Update ()
    {
        CheckCameraMovement();
        //TODO REFACTOR ala PixelateController update()
        if(pixelPanel.position.x < _boundsX)
        {
            if (_depixel)
            {
                _cam.Move(Vector3.right, panSpeed);
                pixelPanel.transform.Translate(Vector3.right * scanSpeed);
            }
            else if (!_depixel && (pixelPanel.position.x > pixelPanel.rect.xMax))
            {
                _cam.Move(Vector3.left, panSpeed);
                pixelPanel.transform.Translate(Vector3.left * scanSpeed);
            }
        }
        else
        {
            CompleteScan();
        }
	}

    public void Scan(bool opt)
    {
        _depixel = opt;
        Debug.Log("SCANNING");
    }

    private void CheckCameraMovement()
    {
        if (_cam.GetCanMove())
        {
            _cam.StartScanPosition();
        }
    }

    private void CompleteScan()
    {
        StartCoroutine(FindItems());
        _cam.SetCanMove(true);
        Debug.Log("SCAN DONE");
    }

    private IEnumerator FindItems()
    {
        foreach (GameObject item in _items)
        {
            Debug.Log("Item named: " + item.name + " is HIDDEN.");
            item.GetComponent<InteractiveItem>().BeginDrawBorder();
            yield return new WaitForSeconds(1);

        }
        gameObject.SetActive(false);
    }
}
