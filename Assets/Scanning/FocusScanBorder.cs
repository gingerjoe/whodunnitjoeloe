﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FocusScanBorder
{
    private CustomLineRenderer _lineRenderer;
    private List<Vector3> borderPoints = new List<Vector3>();

    private Vector3 _itemTopLeft;
    private Vector3 _itemTopRight;
    private Vector3 _itemBottomLeft;
    private Vector3 _itemBottomRight;

    private float _borderThickness;
    private float _borderDrawSpeed;

    private int _currentDrawingIndex = 0;
    private Vector3 _currentPosition;
    private InteractiveItem _interactiveItem;

    public FocusScanBorder(InteractiveItem item)
    {
        Vector3 localScale = item.transform.localScale;
        Vector3 position = item.transform.position;

        _itemTopLeft = new Vector3((-5 * localScale.x) + position.x, (5 * localScale.y) + position.y, -1);
        _itemTopRight = new Vector3((5 * localScale.x) + position.x,(5 * localScale.y) + position.y, -1);
        _itemBottomLeft = new Vector3((-5 * localScale.x) + position.x, (-5 * localScale.y) + position.y, -1);
        _itemBottomRight = new Vector3((5 * localScale.x)  + position.x, (-5 * localScale.y) + position.y, -1);

        _borderThickness = Global.Instance.FocusScanBorderThickness * ((localScale.x + localScale.y)/2);
        _borderDrawSpeed = Global.Instance.FocusScanBorderDrawSpeed;
        _interactiveItem = item;
        CalculateBorderPoints();
        CalculateBannerPoints();

        _currentPosition = borderPoints[0];
        _lineRenderer = new CustomLineRenderer(item.gameObject, borderPoints[0], _borderThickness);
    }

    public void Update(float delta)
    {
        Vector3 targetPosition = borderPoints[_currentDrawingIndex];
        Vector3 nextPosition = Vector3.MoveTowards(_currentPosition, targetPosition, delta * _borderDrawSpeed);

        _lineRenderer.UpdatePoint(nextPosition);
        _currentPosition = nextPosition;

        if (Vector3.Distance(_currentPosition, targetPosition) < 0.01f)
        {
            _currentDrawingIndex += 1;

            if (_currentDrawingIndex == borderPoints.Count)
            {
                _interactiveItem.EndDrawBorder();
            }
            else
            {
                _lineRenderer.AddLine();
            }
        }
    }

    public void CalculateBorderPoints()
    {
        Vector3 startPoint = _itemTopRight;
        startPoint.y += _borderThickness * 4;

        borderPoints.Add(startPoint);

        borderPoints.Add(_itemBottomRight);

        borderPoints.Add(_itemBottomLeft);

        Vector3 newPoint1 = new Vector3(_itemTopLeft.x, _itemTopLeft.y + _borderThickness, _itemTopLeft.z);
        borderPoints.Add(newPoint1);

        Vector3 newPoint2 = new Vector3(_itemTopRight.x, _itemTopRight.y + _borderThickness, _itemTopRight.z);
        borderPoints.Add(newPoint2);
    }

    public void CalculateBannerPoints()
    {
        Vector3 newPoint1 = new Vector3(_itemTopRight.x, _itemTopRight.y + (_borderThickness * 2), _itemTopRight.z);

        Vector3 newPoint2 = new Vector3(_itemTopLeft.x, _itemTopLeft.y + (_borderThickness * 2), _itemTopLeft.z);

        Vector3 newPoint3 = new Vector3(_itemTopLeft.x, _itemTopLeft.y + (_borderThickness * 3), _itemTopLeft.z);

        Vector3 newPoint4 = new Vector3(_itemTopRight.x, _itemTopRight.y + (_borderThickness * 3), _itemTopRight.z);

        Vector3 newPoint5 = new Vector3(_itemTopRight.x, _itemTopRight.y + (_borderThickness * 4), _itemTopRight.z);

        Vector3 newPoint6 = new Vector3(_itemTopLeft.x, _itemTopLeft.y + (_borderThickness * 4), _itemTopLeft.z);

        Vector3 newPoint7 = new Vector3(_itemTopLeft.x, _itemTopLeft.y + _borderThickness, _itemTopLeft.z);

        borderPoints.Add(newPoint1);
        borderPoints.Add(newPoint2);
        borderPoints.Add(newPoint3);
        borderPoints.Add(newPoint4);
        borderPoints.Add(newPoint5);
        borderPoints.Add(newPoint6);
        borderPoints.Add(newPoint7);
    }

    public void SetColor(Color color)
    {
        _lineRenderer.SetColor(color);
    }
}