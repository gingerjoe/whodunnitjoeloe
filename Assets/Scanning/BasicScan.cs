﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BasicScan : MonoBehaviour {

    public RectTransform _basicTooltip;
    public Camera _camera;

    public GameObject title;

    public Vector3 objectPos;
    public Vector2 _screenPos;

    // Use this for initialization
    void Start () {

        _basicTooltip = GameObject.Find("BasicScanUI/" + gameObject.name + "BasicDetail").GetComponent<RectTransform>();
        Debug.Log("GETTING BASIC SCAN: " + _basicTooltip.name);
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        title = _basicTooltip.transform.FindChild("Title").gameObject;
        title.GetComponent<Text>().text = gameObject.name;

        Vector3 objectPos = _camera.WorldToScreenPoint(gameObject.transform.position);
        _screenPos = new Vector2(objectPos.x, objectPos.y);

        if (_basicTooltip == null) {
            Debug.LogWarning("Object: " + gameObject.name + " has no basic tooltip prefab. Check correct location and naming convention matches BasicScanUI/<ObjectName>BasicDetail");
        }
        _basicTooltip.gameObject.SetActive(false);

	}
	
    public void DisplayBasicTooltip()
    {
        _basicTooltip.gameObject.SetActive(true);
        _basicTooltip.position = _screenPos;
    }

    public void HideBasicTooltip()
    {
        _basicTooltip.gameObject.SetActive(false);
    }
}
