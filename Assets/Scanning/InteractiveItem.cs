﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using StateManagement;

public class InteractiveItem : MonoBehaviour
{
    private bool _drawing = false;
    private FocusScanBorder _border;
    private readonly StateMachine<ItemState, ItemTrigger> _itemStateMachine;
    private PixelateController _pixelateController;
    private GameManager _gameManager;

    private enum ItemState
    {
        Hidden,
        FocusScanInitialising,
        FocusScanReady,
        Scanning,
        Visible,
        Inspection
    }

    public enum ItemTrigger
    {
        InitFocusScan,
        FocusScanInitialised,
        Interact,
        FinishFocusScan,
        Hover,
        UnHover
    }

    public InteractiveItem()
    {
        _pixelateController = GetComponent<PixelateController>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        _itemStateMachine = new StateMachine<ItemState, ItemTrigger>(ItemState.Hidden);

        _itemStateMachine.Configure(ItemState.Hidden)
            .Permit(ItemTrigger.InitFocusScan, ItemState.FocusScanInitialising);

        _itemStateMachine.Configure(ItemState.FocusScanInitialising)
            .OnEnter((t) => BeginDrawBorder())
            .Permit(ItemTrigger.FocusScanInitialised, ItemState.FocusScanReady);

        _itemStateMachine.Configure(ItemState.FocusScanReady)
            .Permit(ItemTrigger.Interact, ItemState.Scanning);

        _itemStateMachine.Configure(ItemState.Scanning)
            .OnEnter((t) => BeginScanning())
            .OnExit((t) => ItemIdentified())
            .Permit(ItemTrigger.FinishFocusScan, ItemState.Visible);

        _itemStateMachine.Configure(ItemState.Visible)
            .Permit(ItemTrigger.Hover, ItemState.Inspection);

        _itemStateMachine.Configure(ItemState.Inspection)
            .Permit(ItemTrigger.UnHover, ItemState.Visible);
    }

    void OnMouseDown() {
        GameManager.Instance.InteractItem(this.gameObject);
    }

    private void ItemIdentified()
    {
        _border.SetColor(Color.green);
    }

    void Start()
    {
        _border = new FocusScanBorder(this);
    }

    void Update()
    {
        if (!_drawing) return;

        _border.Update(Time.deltaTime);
    }

    public void BeginDrawBorder()
    {
        _drawing = true;
    }

    public void EndDrawBorder()
    {
        _drawing = false;
    }

    public void BeginScanning()
    {
        if (_drawing) return;

        _pixelateController.RemovePixelation();
    }

    public void Fire(ItemTrigger trigger)
    {
        _itemStateMachine.Fire(trigger);
    }
}
