﻿using UnityEngine;
using System.Collections;

public class CameraPanning : MonoBehaviour {

    public Transform background;
    public bool dragControl;
    public bool canMove;
    public int edgeBoundaries = 100;
    public float sensitivity = 1.0f;

    private Camera _cam;
    private float _backgroundX, _backgroundY;
    private float _verticalExtent, _horizontalExtent;
    private float _minX, _maxX;

	void Start () {
        canMove = true;
        _cam = gameObject.GetComponent<Camera>();
        _backgroundX = background.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        _backgroundY = background.GetComponent<SpriteRenderer>().sprite.bounds.size.y;

        _cam.orthographicSize = _backgroundY / 2.0f;

        _verticalExtent = _cam.orthographicSize;
        _horizontalExtent = _verticalExtent * Screen.width / Screen.height;
        //Debug.Log("Background size y: " + backgroundY);

        _minX = _horizontalExtent - _backgroundX / 2.0f;
        //Debug.Log("MinX: " + _minX.ToString());
        _maxX = _backgroundX / 2.0f - _horizontalExtent;
        //Debug.Log("MaxX: " + _maxX  .ToString());

    }

	void Update () {
        if (canMove)
        {
            if (dragControl)
            {
                DragControl();
            }
            else
            {
                PanControl();
            }
        }
	}

    void LateUpdate()
    {
        Vector3 tempPos = transform.position;

        tempPos.x = Mathf.Clamp(tempPos.x, _minX, _maxX);

        transform.position = tempPos;
    }

    private void PanControl()
    {
        if (Input.mousePosition.x < edgeBoundaries)
        {
            Move(Vector3.left, sensitivity);
        }
        else if (Input.mousePosition.x > Screen.width - edgeBoundaries)
        {
            Move(Vector3.right, sensitivity);
        }
    }

    private void DragControl()
    {
        if (Input.GetMouseButton(0))
        {
            float move = 0;
            float speed = Input.GetAxis("Mouse X");
            move += speed * sensitivity;

            if (move != 0)
            {
                transform.Translate(Vector3.right * move);
            }
        }
    }

    public void Move(Vector3 dir, float speed)
    {
        transform.Translate(dir * speed);
    }

    public bool GetCanMove()
    {
        return canMove;
    }

    public void SetCanMove(bool opt)
    {
        canMove = opt;
    }

    //For the Pan n' Scan (SurfaceScan.cs)
    public void StartScanPosition()
    {
        transform.position = new Vector3(_minX, transform.position.y, transform.position.z);
        SetCanMove(false);
    }

}
