﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AgentController : MonoBehaviour
{
    protected List<Vector3> pathNodes = new List<Vector3>();
    private int currentPathIndex = 0;
    private float pathfindingAccuracy = 0.5f;

    private NavMeshAgent _agent;

    private string objectName;

    protected virtual void Start()
    {
        objectName = gameObject.name;
        _agent = gameObject.GetComponent<NavMeshAgent>();
    }   

    public void MoveAgentTo(Vector3 point)
    {
        Debug.Log("NPC: " + objectName + " moving to: " + point);
        _agent.destination = point;  
    }

    public void UpdateAgentMovement()
    {
        if(_agent.hasPath)
        {
            if (_agent.remainingDistance < pathfindingAccuracy)
                GotoNextNode();
        } else {
            Debug.Log("Begin Patrol for NPC: " + objectName);
            GotoNextNode();
        }
    }

    void GotoNextNode()
    {
        if (pathNodes.Count == 0)
            return;

        if(currentPathIndex < pathNodes.Count)
        {
            MoveAgentTo(pathNodes[currentPathIndex]);
            currentPathIndex += 1;
        } else
        {
            currentPathIndex = 0;
        }
    }
}
