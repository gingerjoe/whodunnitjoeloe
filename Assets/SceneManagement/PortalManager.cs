﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PortalManager : Singleton<PortalManager> {

	public List<string> AllowedTags = new List<string>();

	void Start () {
		Debug.Log("PortalManager initalised");
		DontDestroyOnLoad (this);
	}

}
