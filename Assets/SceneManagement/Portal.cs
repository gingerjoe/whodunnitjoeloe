﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    private const float DistanceToExitPortal = 2f;

    private static GameObject _travellingEntity;
    private static string _persistentPortalName;

    private bool _hasEntityExitedPortal = false;

    public string TargetPortalName;
    public string TargetPortalScene;

    void Start()
    {
        GetComponent<Portal>().enabled = VerifyPortal();
    }

    void Update()
    {
        if (_hasEntityExitedPortal) return;
        CheckIfEntityHasExitedPortal();
    }


    void OnTriggerEnter(Collider collider)
    {
        if (!_hasEntityExitedPortal) return;
        Debug.Log("Portal Entered: " + this.name);

        GameObject collisionObject = collider.gameObject;
        if (IsObjectAllowedInPortal(collisionObject))
        {
            HandleObjectEnteringPortal(collisionObject);
        }
    }

    private void HandleObjectEnteringPortal(GameObject objectEnteringPortal)
    {
        Debug.Log("GameObject allowed in portal (" + objectEnteringPortal.tag + "), Target scene: " + TargetPortalScene);

        _travellingEntity = objectEnteringPortal;
        _persistentPortalName = TargetPortalName;

        if (IsTargetPortalInActiveScene(TargetPortalScene))
        {
            MoveEntityToTargetPortal();
        }
        else
        {
            StartCoroutine(LoadTargetScene());
        }
    }

    IEnumerator LoadTargetScene()
    {
        float fadeTime = TransitionManager.Instance.BeginFade(1);
        yield return new WaitForSeconds(fadeTime);

        DontDestroyOnLoad(_travellingEntity);
        SceneManager.LoadScene(TargetPortalScene);
    }

    private static bool IsTargetPortalInActiveScene(string targetPortalScene)
    {
        return SceneManager.GetActiveScene().name == targetPortalScene;
    }

    private static bool IsObjectAllowedInPortal(GameObject gameObject)
    {
        return PortalManager.Instance.AllowedTags.Any(tag => tag == gameObject.tag);
    }

    void OnLevelWasLoaded(int level)
    {
        Debug.Log("Portal: Target level loaded");
        if (_travellingEntity == null)
        {
            Debug.Log("travellingGameObject Lost");
            return;
        }

        DestroySingletonDuplications();

        MoveEntityToTargetPortal();
    }

    private static void DestroySingletonDuplications()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag(_travellingEntity.tag);
        if (objects.Length <= 1) return;

        foreach (GameObject obj in objects)
        {
            if (obj != _travellingEntity)
            {
                Destroy(obj);
            }
        }
    }

    private void MoveEntityToTargetPortal()
    {
        GameObject target = GameObject.Find(_persistentPortalName);
        target.GetComponent<Portal>()._hasEntityExitedPortal = false;

        float groundLevel = target.transform.position.y + Global.PlayerHeight; //TODO Get height from current player
        _travellingEntity.transform.position = new Vector3(target.transform.position.x, groundLevel, target.transform.position.z);
    }

    private void CheckIfEntityHasExitedPortal()
    {
        GameObject portal = gameObject;
        GameObject entity = GameObject.FindGameObjectWithTag("Player"); //TODO Update Player reference when we know how player will work
        if (Vector3.Distance(portal.transform.position, entity.transform.position) > DistanceToExitPortal)
        {
            _hasEntityExitedPortal = true;
        }
    }

    private bool VerifyPortal()
    {
        if (GetComponent<Collider>() == null)
        {
            throw new MissingComponentException("Portal: " + this.name + " requires a collider attached");
        }
        else if (!GetComponent<Collider>().isTrigger)
        {
            GetComponent<Collider>().isTrigger = true;
        }

        if (TargetPortalName == "")
        {
            Debug.LogError("Portal: " + this.name + " has no target portal");
            return false;
        }
        else if (TargetPortalScene == "")
        {
            Debug.LogError("Portal: " + this.name + " has no target scene");
            return false;
        }

        return true;
    }
}