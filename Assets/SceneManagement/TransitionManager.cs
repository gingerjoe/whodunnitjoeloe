﻿using System;
using UnityEngine;
using System.Collections;

public class TransitionManager : Singleton<TransitionManager>
{
    public Texture2D FadeOutTexture;
    public float FadeSpeed = 0.8f;

    private int _drawDepth = -1000;
    private float _alpha = 0.0f;
    private int _fadeDirection = -1;


    void OnGUI()
    {
        _alpha += _fadeDirection * FadeSpeed * Time.deltaTime;
        _alpha = Mathf.Clamp01(_alpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, _alpha);
        GUI.depth = _drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeOutTexture, ScaleMode.StretchToFill);
    }

    public float BeginFade(int direction)
    {
        _fadeDirection = direction;
        return (FadeSpeed);
    }

    void OnLevelWasLoaded()
    {
        _alpha = 1;
        BeginFade(-1);
    }
}