﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using UnityEditorInternal;
using StateManagement;

public class StateMachineTests
{
    private enum State { A, B, C }

    private enum Trigger { X, Y, Z }
		
    [Test]
    public void InitialStateEqualsCurrentState()
    {
        State initialState = State.A;
        StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(initialState);
		Assert.AreEqual(initialState, stateMachine.CurrentState);
    }

	[Test]
	public void WhenNonPermittedTriggerIsFired_NoStateChangeOccurs() 
	{
		State initialState = State.A;
		Trigger nonPermittedTrigger = Trigger.X;
		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(initialState);

		stateMachine.Fire (nonPermittedTrigger);

		Assert.AreEqual (initialState, stateMachine.CurrentState);
	}

	[Test]
	public void WhenPermittedTriggerIsFired_CurrentStateEqualsTarget()
	{
		State initialState = State.A;
		State targetState = State.B;
		Trigger permittedTrigger = Trigger.X;
		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(initialState);
		stateMachine.Configure (initialState).Permit (permittedTrigger, targetState);

		stateMachine.Fire (permittedTrigger);

		Assert.AreNotEqual (initialState, stateMachine.CurrentState);
		Assert.AreEqual (targetState, stateMachine.CurrentState);
	}

	[Test]
	public void WhenEnterBehaviourDefined_OccursOnEnteringNewState() 
	{
		bool onEnterCalled = false;

		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.A);
		stateMachine.Configure (State.A)
			.Permit (Trigger.X, State.B);

		stateMachine.Configure (State.B)
			.OnEnter ( t => { onEnterCalled = true; } );
	
		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.B, stateMachine.CurrentState);
		Assert.IsTrue (onEnterCalled);
	}

	[Test]
	public void WhenExitBehaviourDefined_OccursOnExitingCurrentState() 
	{
		bool onExitCalled = false;

		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.A);
		stateMachine.Configure (State.A)
			.OnExit( t => { onExitCalled = true; } )
			.Permit (Trigger.X, State.B);

		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.B, stateMachine.CurrentState);
		Assert.IsTrue (onExitCalled);
	}

	[Test]
	public void NoEnterBehaviourDefined_IsAllowed()
	{
		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.A);
		stateMachine.Configure (State.A)
			.OnExit (t => {})
			.Permit (Trigger.X, State.B);
		
		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.B, stateMachine.CurrentState);
	}

	[Test]
	public void NoExitBehaviourDefined_IsAllowed()
	{
		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.A);
		stateMachine.Configure (State.A)
			.OnEnter (t => {})
			.Permit (Trigger.X, State.B);

		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.B, stateMachine.CurrentState);
	}

	[Test]
	public void NoEnterOrExitBehaviourDefined_IsAllowed()
	{
		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.A);
		stateMachine.Configure (State.A)
			.Permit (Trigger.X, State.B);

		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.B, stateMachine.CurrentState);
	}

	[Test]
	public void MultiplePermitsDefined_IsAllowed() 
	{
		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.A);
		stateMachine.Configure (State.A)
			.OnEnter (t => {})
			.OnExit (t => {})
			.Permit (Trigger.X, State.B)
			.Permit (Trigger.Y, State.C);

		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.B, stateMachine.CurrentState);
	}

	[Test]
	public void MultiplePermitsAndStateTransitions_IsAllowed() 
	{
		StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.A);
		stateMachine.Configure (State.A)
			.OnEnter (t => {})
			.OnExit (t => {})
			.Permit (Trigger.X, State.B)
			.Permit (Trigger.Y, State.C);

		stateMachine.Configure (State.B)
			.OnEnter (t => {})
			.OnExit (t => {})
			.Permit (Trigger.X, State.C)
			.Permit (Trigger.Y, State.C);

		Assert.AreEqual (State.A, stateMachine.CurrentState);
		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.B, stateMachine.CurrentState);
		stateMachine.Fire (Trigger.X);
		Assert.AreEqual (State.C, stateMachine.CurrentState);
	}
}
