﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using StateManagement;

public class StateRepresentationTests {

	private enum State { A, B, C }

	private enum Trigger { X, Y, Z }

	StateMachine<State, Trigger>.Transition genericTransition = new StateMachine<State, Trigger>.Transition (State.A, State.B, Trigger.X);

	[Test]
	public void WhenBehaviourExists_TriggerCanBeFired()
	{
		Trigger triggerToBeFired = Trigger.X;
		StateMachine<State, Trigger>.StateRepresentation stateRepresentation 
				= new StateMachine<State, Trigger>.StateRepresentation (State.A);
		stateRepresentation.AddTriggerBehaviour (new StateMachine<State, Trigger>.TriggerBehaviour (triggerToBeFired, State.B));

		Assert.IsTrue(stateRepresentation.CanHandle(triggerToBeFired));
	}

	[Test]
	public void WhenBehaviourDoesNotExist_TriggerCanNotBeFired()
	{
		StateMachine<State, Trigger>.StateRepresentation stateRepresentation 
				= new StateMachine<State, Trigger>.StateRepresentation (State.A);

		Assert.IsFalse(stateRepresentation.CanHandle(Trigger.X));
	}

	[Test]
	public void OnEnter_EnterBehaviourExecuted()
	{
		bool onEnterCalled = false;
		StateMachine<State, Trigger>.StateRepresentation stateRepresentation 
				= new StateMachine<State, Trigger>.StateRepresentation (State.A);
		stateRepresentation.AddEntryBehavior (t => {
			onEnterCalled = true;
		});

		stateRepresentation.Enter(genericTransition);

		Assert.IsTrue (onEnterCalled);
	}

	[Test]
	public void OnEnter_ExitBehaviourIsNotExecuted()
	{
		bool onExitCalled = false;
		StateMachine<State, Trigger>.StateRepresentation stateRepresentation 
				= new StateMachine<State, Trigger>.StateRepresentation (State.A);
		stateRepresentation.AddExitBehavior (t => {
			onExitCalled = true;
		});

		stateRepresentation.Enter(genericTransition);

		Assert.IsFalse (onExitCalled);
	}

	[Test]
	public void OnExit_ExitBehaviourExecuted()
	{
		bool onExitCalled = false;
		StateMachine<State, Trigger>.StateRepresentation stateRepresentation 
				= new StateMachine<State, Trigger>.StateRepresentation (State.A);

		stateRepresentation.AddExitBehavior (t => {
			onExitCalled = true;
		});

		stateRepresentation.Exit(genericTransition);

		Assert.IsTrue (onExitCalled);
	}

	[Test]
	public void OnExit_EnterBehaviourIsNotExecuted()
	{
		bool onEnterCalled = false;
		StateMachine<State, Trigger>.StateRepresentation stateRepresentation 
				= new StateMachine<State, Trigger>.StateRepresentation (State.A);
		stateRepresentation.AddEntryBehavior (t => {
			onEnterCalled = true;
		});

		stateRepresentation.Exit(genericTransition);

		Assert.IsFalse (onEnterCalled);
	}
}
