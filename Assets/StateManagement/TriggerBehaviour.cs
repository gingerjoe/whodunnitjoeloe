using System;
using System.Collections.Generic;

namespace StateManagement
{
    public partial class StateMachine<TState, TTrigger>
    {
        public class TriggerBehaviour
        {
            public TTrigger Trigger { get; set; }

            internal TState Destination { get; set; }

            public TriggerBehaviour(TTrigger trigger, TState destination)
            {
                Trigger = trigger;
                Destination = destination;
            }

        }
    }
}