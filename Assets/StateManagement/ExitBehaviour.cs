﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateManagement
{
    public partial class StateMachine<TState, TTrigger>
    {
        internal class ExitBehaviour
        {
            public ExitBehaviour(Action<Transition> action)
            {
                Action = action;
            }

            internal Action<Transition> Action { get; set;  }
        }
    }
}