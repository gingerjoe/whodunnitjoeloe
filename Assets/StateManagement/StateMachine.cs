using System;
using System.Collections.Generic;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace StateManagement
{
    /// <summary>
    /// Stores the current state of the machine and handles switching between different state configurations.
    /// </summary>
    public partial class StateMachine<TState, TTrigger>
    {
        private readonly IDictionary<TState, StateRepresentation> _stateConfiguration = new Dictionary<TState, StateRepresentation>();

        public TState CurrentState { get; set; }

        private StateMachine(){}

        public StateMachine(TState initialState) : this()
        {
            CurrentState = initialState;
        }

        public StateConfiguration Configure(TState state)
        {
            return new StateConfiguration(this, GetRepresentation(state));
        }

        private StateRepresentation GetCurrentRepresentation()
        {
            return GetRepresentation(CurrentState);
        }

        private StateRepresentation GetRepresentation(TState state)
        {
            StateRepresentation result;
            if (_stateConfiguration.TryGetValue(state, out result)) return result;

            result = new StateRepresentation(state);
            _stateConfiguration.Add(state, result);

            return result;
        }

        public void Fire(TTrigger trigger)
        {
            Debug.Log("Firing trigger: " + trigger);
            StateRepresentation representativeState = GetCurrentRepresentation();

            TriggerBehaviour triggerBehaviour;
            if (!representativeState.TryFindTriggerBehaviour(trigger, out triggerBehaviour))
            {
                Debug.LogError("Current State (" + CurrentState + ") cannot handle Trigger: " + trigger);
                return;
            }

            TState destination = triggerBehaviour.Destination;
            Transition transition = new Transition(CurrentState, destination, trigger);

            representativeState.Exit(transition);

            CurrentState = transition.Destination;
            var newRepresentation = GetCurrentRepresentation();

            newRepresentation.Enter(transition);
        }
    }
}