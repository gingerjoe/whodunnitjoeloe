﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateManagement
{
    public partial class StateMachine<TState, TTrigger>
    {
        public class Transition
        {
            public TState Source { get; set; }
            public TState Destination { get; set; }
            public TTrigger Trigger { get; set; }

            public Transition(TState source, TState destination, TTrigger trigger)
            {
                Source = source;
                Destination = destination;
                Trigger = trigger;
            }
        }
    }
}