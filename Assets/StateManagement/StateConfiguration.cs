using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateManagement
{
    public partial class StateMachine<TState, TTrigger>
    {
        /// <summary>
        /// Used as a controller to define and chain together new configurations for potential states.
        /// This currently includes
        ///     - Triggers for switching to another state
        ///     - Entry & Exit functions for when a state changes
        /// </summary>
        public class StateConfiguration
        {
            readonly StateRepresentation _representation;

            internal StateConfiguration(StateMachine<TState, TTrigger> machine, StateRepresentation representation)
            {
                Machine = machine;
                _representation = representation;
            }

            public TState State
            {
                get { return _representation.State; }
            }

            public StateMachine<TState, TTrigger> Machine { get; set; }

            public StateConfiguration Permit(TTrigger trigger, TState destinationState)
            {
                if (destinationState.Equals(_representation.State))
                {
                    throw new ArgumentException("Cannot transition to identity state");
                }

                _representation.AddTriggerBehaviour(new TriggerBehaviour(trigger, destinationState));
                return this;
            }

            public StateConfiguration OnEnter(Action<Transition> action)
            {
                _representation.AddEntryBehavior(action);
                return this;
            }

            public StateConfiguration OnExit(Action<Transition> action)
            {
                _representation.AddExitBehavior(action);
                return this;
            }
        }
    }

}