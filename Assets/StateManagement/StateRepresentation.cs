using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;

namespace StateManagement
{
    public partial class StateMachine<TState, TTrigger>
    {
        /// <summary>
        /// Stores the allowed Triggers and Entry/Exit functions for the given state.
        /// </summary>
        public class StateRepresentation
        {
            public TState State { get; set; }

            private readonly IDictionary<TTrigger, TriggerBehaviour> _triggerBehaviours = new Dictionary<TTrigger, TriggerBehaviour>();
            internal IDictionary<TTrigger, TriggerBehaviour> TriggerBehaviours
            {
                get { return _triggerBehaviours; }
            }

            internal EntryBehaviour EntryBehaviour { get; private set; }

            internal ExitBehaviour ExitBehaviour { get; private set; }

            public StateRepresentation(TState state)
            {
                State = state;
            }

            public bool CanHandle(TTrigger trigger)
            {
                TriggerBehaviour unused;
                return TriggerBehaviours.TryGetValue(trigger, out unused);
            }

            public void AddTriggerBehaviour(TriggerBehaviour triggerBehaviour)
            {
                TriggerBehaviour existingBehaviour;
                if (TriggerBehaviours.TryGetValue(triggerBehaviour.Trigger, out existingBehaviour))
                {
                    Debug.LogError(string.Format("TriggerBehavior for Trigger ({0}) has already been defined: {1}.", triggerBehaviour.Trigger, existingBehaviour.Destination));
                    return;
                }
                TriggerBehaviours.Add(triggerBehaviour.Trigger, triggerBehaviour);
            }

            public void AddEntryBehavior(Action<Transition> action)
            {
                if (EntryBehaviour != null)
                {
                    Debug.LogError(string.Format("Entry Behaviour for State ({0}) has already been defined.", State));
                    return;
                }
                EntryBehaviour = new EntryBehaviour(action);
            }

            public void AddExitBehavior(Action<Transition> action)
            {
                if (ExitBehaviour != null)
                {
                    Debug.LogError(string.Format("Exit Behaviour for State ({0}) has already been defined.", State));
                    return;
                }
                ExitBehaviour = new ExitBehaviour(action);
            }

            public bool TryFindTriggerBehaviour(TTrigger trigger, out TriggerBehaviour triggerBehaviour)
            {
                TriggerBehaviour possible;
                if (!TriggerBehaviours.TryGetValue(trigger, out possible))
                {
                    triggerBehaviour = null;
                    return false;
                }

                triggerBehaviour = possible;
                return triggerBehaviour != null;
            }

            public void Exit(Transition transition)
            {
                if (ExitBehaviour != null)
                {
                    ExitBehaviour.Action(transition);
                }
            }

            public void Enter(Transition transition)
            {
                if (EntryBehaviour != null)
                {
                    EntryBehaviour.Action(transition);
                }
            }
        }
    }
}
