﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Choice : MonoBehaviour
{
    private ConversationManager _conversationManager;
    private int _index;

    public void Init(ConversationManager conversationManager, string text, int index)
    {
        _conversationManager = conversationManager;
        gameObject.GetComponentInChildren<Text>().text = text;
        _index = index;

        gameObject.GetComponentInChildren<Button>().onClick.AddListener(delegate { _conversationManager.ChoiceSelected(_index); });
    }
}
