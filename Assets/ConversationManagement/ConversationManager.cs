﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Ink.Runtime;
using StateManagement;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ConversationManager : Singleton<ConversationManager>
{
    private enum State
    {
        Idle,
        AdvanceConversation,
        Narration,
        PlayerTalking,
        AgentTalking,
        Choices
    }

    public enum Trigger
    {
        BeginConversation,
        Continue,
        StartNarration,
        StartPlayerTalking,
        StartAgentTalking,
        DisplayChoices,
        SelectChoice,
        EndConversation
    }

    private StateMachine<State, Trigger> _stateMachine;

    private ConversationViewController _viewController;

    //TODO: Temporary

    private static string _playerText;
    private static string _agentText;
    private static string _narration;

    private TempAgentController _agentController;

    private LinkedList<Choice> _choices;
    private LinkedList<string> _choiceStrings;

    private Story _inkStory;
    private Story _saveInfo;
    private int _choiceIndex;

    private Dictionary<string, ArrayList> _saveVariableNamesToDict;
    private Dictionary<string, ArrayList> _saveTimesToAgentsAndLocations;

    void Start()
    {
        _viewController = GameObject.Find("ConversationUI").GetComponent<ConversationViewController>();
        _viewController.AddNextButtonListener(this);

        SetupStateMachine();
        RemoveConversationView();
    }

    public void BeginConversation(GameObject agent)
    {
        _agentController = agent.GetComponent<TempAgentController>();
        //LoadInkStateFromAgent(agent);
        TextAsset agentTextAsset = agent.GetComponent<AgentInkAttachment>().InkTextAsset;
        _inkStory = new Story(agentTextAsset.text);

        //LoadStoryToInkSave(_inkStory);
        InkManager.Instance.LoadInkVariablesIntoStory(_inkStory);
        InkManager.Instance.CreateExternalFunctionHooks(_inkStory);

        Debug.Log("Begining conversation with: " + _agentController.name);

        Fire(Trigger.BeginConversation);
    }

    private void SaveStoryToInkManager(Story story)
    {
        InkManager.Instance.SaveInkVariablesFromStoryIntoDictionarys(_inkStory);
    }

    public void EndConversation()
    {
        //GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        GameManager.Instance.Fire(GameManager.GameplayTrigger.ExitState);
        //_agentController.SetInteracting(false);
        InkManager.Instance.SaveToFile();
        //InkManagerElement.SaveToFile();
    }

    private void SetupStateMachine()
    {
        _stateMachine = new StateMachine<State, Trigger>(State.Idle);

        _stateMachine.Configure(State.Idle)
            .OnEnter(t => {
                RemoveConversationView();
                SaveStoryToInkManager(_inkStory);
            })
            .Permit(Trigger.BeginConversation, State.AdvanceConversation);

        _stateMachine.Configure(State.AdvanceConversation)
            .OnEnter(t =>
            {
                InitConversationView();
                AdvanceStory();
            })
            .Permit(Trigger.StartNarration, State.Narration)
            .Permit(Trigger.StartPlayerTalking, State.PlayerTalking)
            .Permit(Trigger.StartAgentTalking, State.AgentTalking)
            .Permit(Trigger.DisplayChoices, State.Choices)
            .Permit(Trigger.EndConversation, State.Idle);

        _stateMachine.Configure(State.Narration)
            .OnEnter(t =>
            {
                UpdateNarration(_narration);
                _viewController.EnableNextButton();
            })
            .OnExit(t => _viewController.DisableNextButton())
            .Permit(Trigger.Continue, State.AdvanceConversation);

        _stateMachine.Configure(State.PlayerTalking)
            .OnEnter(t =>
            {
                UpdatePlayerText(_playerText);
                _viewController.EnableNextButton();
            })
            .OnExit(t => _viewController.DisableNextButton())
            .Permit(Trigger.Continue, State.AdvanceConversation);

        _stateMachine.Configure(State.AgentTalking)
            .OnEnter(t =>
            {
                UpdateAgentText(_agentText);
                _viewController.EnableNextButton();
            })
            .OnExit(t => _viewController.DisableNextButton())
            .Permit(Trigger.Continue, State.AdvanceConversation);

        _stateMachine.Configure(State.Choices)
            .OnEnter(t => DisplayChoices())
            .OnExit(t => _inkStory.ChooseChoiceIndex(_choiceIndex))
            .Permit(Trigger.SelectChoice, State.AdvanceConversation);
    }

    private void DisplayChoices()
    {
        _viewController.DisplayChoices(this, _choiceStrings);
        //switch (_choiceStrings.Count)
        //{
        //    case 1:
        //        _choices = _viewController.DisplayOneChoices(this, _choiceStrings);
        //        break;
        //    case 2:
        //        _choices = _viewController.DisplayTwoChoices(this, _choiceStrings);
        //        break;
        //    case 3:
        //        _choices = _viewController.DisplayThreeChoices(this, _choiceStrings);
        //        break;
        //    case 4:
        //        _choices = _viewController.DisplayFourChoices(this, _choiceStrings);
        //        break;
        //    case 5:
        //        _choices = _viewController.DisplayFiveChoices(this, _choiceStrings);
        //        break;
        //    case 6:
        //        _choices = _viewController.DisplaySixChoices(this, _choiceStrings);
        //        break;
        //    default:
        //        throw new NotSupportedException(_choiceStrings.Count + " choice(s) are not supported");
        //}
    }

    private void UpdateAgentText(string agentText)
    {
        _viewController.DisplayAgentText(_agentController.name, agentText);
    }

    private void UpdatePlayerText(string playerText)
    {
        _viewController.DisplayPlayerText(playerText);
    }

    private void UpdateNarration(string narrativeText)
    {
        _viewController.DisplayNarrativeText(narrativeText);
    }

    private void InitConversationView()
    {
        _viewController.EnableView();
        _viewController.DisableChoices();
    }

    private void RemoveConversationView()
    {
        _viewController.DisableView();
        if (_agentController != null)
        {
            EndConversation();
        }
    }

    public void Fire(Trigger trigger)
    {
        _stateMachine.Fire(trigger);
    }

    public void AdvanceStory()
    {
        if (_inkStory.canContinue)
        {
            HandleSpeech();
        }
        else
        {
            HandleChoices();
        }
    }

    private void HandleSpeech()
    {
        string nextText = _inkStory.Continue();
        string[] separatedText = nextText.Split(':');
        string playerName = Global.Instance.GetPlayerName();

        if (separatedText.Length == 0) return;
        if (separatedText.Length == 1)
        {
            Debug.Log("Displaying narrative text: " + nextText);
            AddNarrativeText(nextText);
            //DisplayNarrativeText(nextText);
            return;
        }

        string name = separatedText[0];
        string text = separatedText[1];
        if (IsPlayer(nextText))
        {
            AddPlayerText(text);
            //DisplayPlayerText(playerName, text);
        }
        else
        {
            AddAgentText(text);
            //DisplayAgentText(_agentController.name, text);
        }
    }

    private bool IsPlayer(string nextText)
    {
        return nextText.StartsWith(Global.Instance.GetPlayerPrefix() + ":");
    }

    //TODO Pass variables into triggers when option exists
    private void AddNarrativeText(string text)
    {
        _narration = text;
        //_playerText = text;
        Fire(Trigger.StartNarration); // TODO Trigger.StartNarration?
    }

    public void AddPlayerText(string playerText)
    {
        _playerText = playerText;
        Fire(Trigger.StartPlayerTalking);
    }

    public void AddAgentText(string agentText)
    {
        _agentText = agentText;
        Fire(Trigger.StartAgentTalking);
    }

    private void HandleChoices()
    {
        if (_inkStory.currentChoices.Count > 0)
        {
            _choiceStrings = new LinkedList<string>();
            foreach (Ink.Runtime.Choice c in _inkStory.currentChoices)
            {
                _choiceStrings.AddLast(c.text);
            }
            AddChoices(_choiceStrings);
        }
        else
        {
            Debug.Log("Ending Conversation with: " + _agentController.name);
            Fire(Trigger.EndConversation);
        }
    }

    public void AddChoices(LinkedList<string> choices)
    {
        _choiceStrings = choices;
        Fire(Trigger.DisplayChoices);
    }

    public void OnNextClicked()
    {
        Debug.Log("Next Button clicked");
        Fire(Trigger.Continue);
    }

    //public void DisplayPlayerText(string playerName, string playerText)
    //{
    //    string textToDisplay = "<b>" + playerName + "</b>\n" + "<i>" + playerText + "</i>";
    //    AddPlayerText(textToDisplay);
    //}

    //public void DisplayNarrativeText(string text)
    //{
    //    string textToDisplay = "<b><i><color=cyan>" + text + "</color></b></i>";
    //    AddPlayerText(textToDisplay);
    //}

    //public void DisplayAgentText(string agentName, string agentText)
    //{
    //    string textToDisplay = "<b>" + agentName + "</b>\n" + agentText;
    //    AddAgentText(textToDisplay);
    //}

    // For use in Choice.cs delegate button function

    public void ChoiceSelected(int index)
    {
        _choiceIndex = index;
        Fire(Trigger.SelectChoice);
    }
}
