﻿using UnityEngine;
using System.Collections;

public class TempAgentController : MonoBehaviour
{
    private ConversationManager _convoManager;
    private bool _isInteracting = false;

	// Use this for initialization
	void Start ()
	{
	    _convoManager = GameObject.Find("ConvoManager").GetComponent<ConversationManager>();
	}

    void OnMouseDown()
    {
        //GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        GameManager.Instance.InteractNpc(this.gameObject);

        // if (!_isInteracting)
        // {
        //     _convoManager.BeginConversation(gameObject);
        //     _isInteracting = true;
        // }
    }

    public void SetInteracting(bool interacting)
    {
        _isInteracting = interacting;
    }

}
