﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Ink.Runtime;

public class ConversationViewController : MonoBehaviour
{
    public GameObject PlayerTextBox;
    public GameObject AgentTextBox;
    public GameObject ChoicesButtons;

    public Transform LayoutManagerParentTransform;

    private Button _nextButton;

    public float playerDelayBetweenWordsInSeconds = 0.1f;
    public float agentDelayBetweenWordsInSeconds = 0.2f;
    public float narrationDelayBetweenWordsInSeconds = 0.1f;

    private int _numChoices = 0;

    private Canvas _thisCanvas;

    void Awake()
    {
        Verify();

        _thisCanvas = gameObject.GetComponent<Canvas>();

        _nextButton = gameObject.transform.FindChild("NextButton").GetComponent<Button>();

        //PlayerTextBox = PlayerTextBox.GetComponent<Text>();
        //AgentTextBox = AgentTextBox.GetComponent<Text>();
    }

    private void Verify()
    {
        if (PlayerTextBox == null)
        {
            throw new MissingComponentException("Player text box was not found");
        }
        if (AgentTextBox == null)
        {
            throw new MissingComponentException("Agent text box was not found");
        }
    }

    public void EnableNextButton()
    {
        _nextButton.gameObject.SetActive(true);
    }

    public void DisableNextButton()
    {
        _nextButton.gameObject.SetActive(false);
    }

    public void EnableView()
    {
        _thisCanvas.enabled = true;
    }

    public void DisableView()
    {
        _thisCanvas.enabled = false;
    }

    public void AddNextButtonListener(ConversationManager conversationManager)
    {
        _nextButton.onClick.AddListener(conversationManager.OnNextClicked);
    }

    public void DisableChoices()
    {
        for (int i = 0; i < _numChoices; i++)
        {
            LayoutManagerParentTransform.GetChild(LayoutManagerParentTransform.childCount - i - 1).gameObject.SetActive(false); //DANGER high voltage
        }
    }

    public void DisplayPlayerText(string textToDisplay)
    {
        _numChoices = 0;
        //TODO ticker-text
        //PlayerTextBox = textToDisplay;

        GameObject playerText = Instantiate(PlayerTextBox);
        Text playerTextElement = playerText.GetComponentInChildren<Text>();

        playerTextElement.text = textTagger(Global.Instance.GetPlayerName(), bold:true) + "\n";

        _activeAnimation = AnimateText(playerTextElement, textToDisplay, playerDelayBetweenWordsInSeconds);

        playerText.transform.SetParent(LayoutManagerParentTransform);
        playerText.SetActive(true);

        StartCoroutine(_activeAnimation);
    }


    public void DisplayAgentText(string agentName, string textToDisplay)
    {
        _numChoices = 0;
        //AgentTextBox.text = textToDisplay;

        GameObject agentText = Instantiate(AgentTextBox);
        Text agentTextElement = agentText.GetComponentInChildren<Text>();

        agentTextElement.text = textTagger(agentName, bold:true) + "\n";

        _activeAnimation = AnimateText(agentTextElement, textToDisplay, agentDelayBetweenWordsInSeconds);
        //StartCoroutine(AnimateText(AgentTextBox, textToDisplay, agentDelayBetweenWordsInSeconds));

        agentText.transform.SetParent(LayoutManagerParentTransform);
        agentText.SetActive(true);

        StartCoroutine(_activeAnimation);
    }

    public void DisplayNarrativeText(string textToDisplay)
    {
        _numChoices = 0;

        GameObject playerText = Instantiate(PlayerTextBox);
        Text playerTextElement = playerText.GetComponentInChildren<Text>();

        playerTextElement.text = "\n"; //FIXME fix narration to its own textbox

        _activeAnimation = AnimateNarration(playerTextElement, textToDisplay, narrationDelayBetweenWordsInSeconds);

        playerText.transform.SetParent(LayoutManagerParentTransform);
        playerText.SetActive(true);

        StartCoroutine(_activeAnimation);
    }

    private bool _animatingText = false;
    private IEnumerator _activeAnimation;
    private Text _animationTextObj;
    private string _animationBackupString;
    void Update()
    {
        if (_animatingText)
        {
            if (Input.anyKeyDown)
            {
                Debug.LogWarning("Ending text early");
                StopCoroutine(_activeAnimation);
                _animationTextObj.text = _animationBackupString;
                _animatingText = false;
            }
        }
    }

    IEnumerator AnimateText(Text textObject, string text, float delayBetweenWords)
    {
        string[] textToDisplay = text.Split();
        string oldText = textObject.text;
        string newText = oldText;

        _animationTextObj = textObject;
        _animationBackupString = oldText + textTagger(text, italic: true);

        //_animatingText = true;

        for (int i = 0; i < textToDisplay.Length; i++)
        {
            if(i > 1) _animatingText = true;

            string currentWord = textToDisplay[i];
            foreach(char c in currentWord)
            {
                newText += c;
                textObject.text = textTagger(newText, italic: true);
                float delay = 1 / (currentWord.Length / delayBetweenWords);
                yield return new WaitForSeconds(delay);
            }

            if (i != textToDisplay.Length - 1) newText += " ";
        }

        _animatingText = false;
    }

    IEnumerator AnimateNarration(Text textObject, string text, float delayBetweenWords)
    {
        string[] textToDisplay = text.Split();
        string oldText = textObject.text;
        string newText = oldText;

        _animationTextObj = textObject;
        _animationBackupString = oldText + textTagger(text, bold: true, italic: true, color: true, colorValue: "red");

        //_animatingText = true;

        for (int i = 0; i < textToDisplay.Length; i++)
        {
            if (i > 1) _animatingText = true;

            string currentWord = textToDisplay[i];
            foreach (char c in currentWord)
            {
                newText += c;
                textObject.text = textTagger(newText, bold: true, italic: true, color: true, colorValue: "red");
                float delay = 1 / (currentWord.Length / delayBetweenWords);
                yield return new WaitForSeconds(delay);
            }

            if (i != textToDisplay.Length - 1) newText += " ";
        }

        _animatingText = false;
    }

    public string textTagger(string text, bool bold=false, bool italic=false, bool color=false, string colorValue = null)
    {
        string tagsAtFront = "";
        string tagsAtEnd = "";

        if (bold)
        {
            tagsAtFront += "<b>";
            tagsAtEnd += "</b>";
        }
        if (italic)
        {
            tagsAtFront += "<i>";
            tagsAtEnd = "</i>" + tagsAtEnd;
        }
        if (color && colorValue != null)
        {
            tagsAtFront += string.Concat("<color=", colorValue, ">");
            tagsAtEnd = "</color>" + tagsAtEnd;
        }

        return string.Concat(tagsAtFront, text, tagsAtEnd);
    }


    public LinkedList<Choice> DisplayChoices(ConversationManager conversationManager, LinkedList<string> choiceStrings)
    {
        //choiceParent.SetActive(true);
        LinkedList<Choice> choices = new LinkedList<Choice>();
        LinkedListNode<string> currentString = choiceStrings.First;
        _numChoices = choiceStrings.Count;
        for (int i = 0; i < _numChoices; i++)
        {
            GameObject gg = Instantiate(ChoicesButtons);
            AssignChoiceComponentToViewChoice(conversationManager, gg, currentString.Value, i);
            gg.transform.SetParent(LayoutManagerParentTransform);

            currentString = currentString.Next;
        }
        return choices;
    }

    private void AssignChoiceComponentToViewChoice(ConversationManager conversationManager, GameObject choiceParent, string currentString, int choiceIndex)
    {
        //Transform child = choiceParent.transform.GetChild(index);
        Choice componentChoice = choiceParent.AddComponent<Choice>();
        componentChoice.Init(conversationManager, currentString, choiceIndex);
    }
}