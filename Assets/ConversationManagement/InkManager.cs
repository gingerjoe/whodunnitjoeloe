﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Ink.Runtime;
using StateManagement;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class InkManager : Singleton<InkManager>
{
    private Story _defaultStory;
    public TextAsset SaveFile;

    private Dictionary<string, List<Pair<string>>> _inkAgentsAndLocationsAtTime;
    private Dictionary<string, string> _inkVariables;
    private Dictionary<string, List<String>> _objectives;

    private string timeRangeSeperator = "-";
    private int timeRangeStringLength = 5; // "HH:MM"

    void Awake()
    {
        _defaultStory = new Story(SaveFile.text);
        bool loadedSuccesfully = SaveManager.Instance.LoadFromFile();
        _inkVariables = SaveManager.Instance.InkVariables;
        _objectives = SaveManager.Instance.Objectives;
        _inkAgentsAndLocationsAtTime = SaveManager.Instance.Times;
        if (!loadedSuccesfully)
        {
            SaveInkVariablesFromStoryIntoDictionarys(_defaultStory);
            _objectives = new Dictionary<string, List<string>>();
            _inkAgentsAndLocationsAtTime = new Dictionary<string, List<Pair<string>>>();
        }

    }

    public void CreateExternalFunctionHooks(Story story)
    {
        story.BindExternalFunction("agent_present_at_time_and_location", (string agentName, string locataion, string time) =>
        {
            IsAgentPresentAtTimeAndLocation(agentName, locataion, time);
        });

        story.BindExternalFunction("add_objective", (string objectiveName, string objectiveText) =>
        {
            AddObjective(objectiveName, objectiveText);
        });
        
        story.BindExternalFunction("update_objective", (string objectiveName, string objectiveText) =>
        {
            UpdateObjective(objectiveName, objectiveText);
        });

        story.BindExternalFunction("update_location_status", (string agentName, string locationStatus, string timeRange) =>
        {
            if (timeRange.Contains(timeRangeSeperator))
            {
                string[] strings = timeRange.Split(timeRangeSeperator.ToCharArray());
                string from = strings[0];
                string to = strings[1];
                SaveLocationStatus(agentName, locationStatus, from, to);
            }
            else
            {
                SaveLocationStatus(agentName, locationStatus, timeRange);
            }
        });
    }

    private int IsAgentPresentAtTimeAndLocation(string agentName, string locataion, string time)
    {
        int output = 0;
        Dictionary<string, List<string>> agentsAtLocationAndTime = GetAgentsAtLocationFromTime(time);
        if (agentsAtLocationAndTime.ContainsKey(locataion))
        {
            List<string> agents = agentsAtLocationAndTime[locataion];
            if (agents.Contains(agentName))
            {
                output = 1;
            }
        }
        return output;
    }

    private void AddObjective(string name, string value)
    {
        if (!_objectives.ContainsKey(name))
        {
            _objectives[name] = new List<string>(){value};
        }
        else
        {
            Debug.LogError("Objective: " + name + ", already exists in current context");
        }
    }

    private void UpdateObjective(string name, string value)
    {
        if (_objectives.ContainsKey(name))
        {
            _objectives[name].Add(value);
        }
        else
        {
            Debug.LogError("Objective: " + name + ", does not exists in current context");
        }
    }

    private void SaveLocationStatus(string agentName, string locationStatus, string timeFrom)
    {
        Debug.Log(agentName + locationStatus + timeFrom);
        if (timeFrom.Length == timeRangeStringLength)
        {
            SaveAgentAtLocationAndTime(agentName, locationStatus, timeFrom);
        }
        else
        {
            Debug.LogError("Invalid timeRange length [" + timeFrom + "]");
        }
    }

    private void SaveLocationStatus(string agentName, string locationStatus, string timeFrom, string timeTo)
    {
        Debug.Log(agentName + locationStatus + timeFrom + timeTo);
        if (timeFrom.Length == timeRangeStringLength)
        {
            if (timeTo.Length == timeRangeStringLength)
            {
                DateTime startDate = DateTime.Parse(string.Concat("5/1/2008 ", timeFrom));
                DateTime endDate = DateTime.Parse(string.Concat("5/1/2008 ", timeTo));

                Debug.Log(startDate.ToShortTimeString());
                Debug.Log(endDate.ToShortTimeString());

                foreach (DateTime day in EveryTenMinutes(startDate, endDate))
                {
                    string hour = day.Hour.ToString();
                    string minute = day.Minute.ToString();

                    if (day.Hour < 10)
                    {
                        hour = "0" + hour;
                    }
                    
                    if (day.Minute < 10){
                        minute = "0" + minute;
                    }

                    string hourAndMinute = hour + ":" + minute;
                    Debug.Log("Saving: " + agentName + ", status to: " + locationStatus + ", at time: " + hourAndMinute);
                    SaveAgentAtLocationAndTime(agentName, locationStatus, hourAndMinute);
                }
            }
            else
            {
                Debug.LogError("Invalid timeTo length [" + timeTo + "], should be formated HH:MM");
            }
        }
        else
        {
            Debug.LogError("Invalid timeFrom length [" + timeFrom + "], should be formated HH:MM");
        }  
    }

    public IEnumerable<DateTime> EveryTenMinutes(DateTime start, DateTime end)
    {
        for (var day = start.Date; day.TimeOfDay <= end.TimeOfDay; day = day.AddMinutes(10))
            yield return day;
    }

    public void SaveToFile()
    {
        Debug.Log(string.Concat(_inkVariables==null, _objectives==null, _inkAgentsAndLocationsAtTime==null));

        Debug.LogWarning(string.Concat(SaveManager.Instance == null));

        SaveManager.Instance.SetInkVarsAndObjectivesAndTimes(_inkVariables, _objectives, _inkAgentsAndLocationsAtTime);
        SaveManager.Instance.SaveToFile();
    }

    public void LoadInkVariablesIntoStory(Story story)
    {
        foreach (string variableName in _inkVariables.Keys)
        {
            string variableValue = _inkVariables[variableName];
            story.variablesState[variableName] = variableValue;
        }
    }

    public void SaveInkVariablesFromStoryIntoDictionarys(Story storyToSave)
    {
        _inkVariables = new Dictionary<string, string>();

        VariablesState inkVariables = storyToSave.variablesState;
        foreach (var inkVariable in inkVariables)
        {
            string inkVariableName = inkVariable.ToString();
            string inkVariableValue = storyToSave.variablesState[inkVariableName].ToString();

            _inkVariables[inkVariableName] = inkVariableValue;

            //if (storyToSave != _saveInfo)
            //{
            //    _saveInfo.variablesState[inkVariableName] = inkVariableValue;
            //}
        }
    }

    private void SaveAgentAtLocationAndTime(string agentName, string location, string time)
    {
        Pair<string> nameAndLocation = new Pair<string>(agentName, location);
        if (_inkAgentsAndLocationsAtTime.ContainsKey(time))
        {
            _inkAgentsAndLocationsAtTime[time].Add(nameAndLocation);
        }
        else
        {
            _inkAgentsAndLocationsAtTime[time] = new List<Pair<string>>(){nameAndLocation};
        }
    }

    public Dictionary<string, List<string>> GetAgentsAtLocationFromTime(string time)
    {
        Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();
        if (_inkAgentsAndLocationsAtTime.ContainsKey(time))
        {
            List<Pair<string>> namesAndLocations = _inkAgentsAndLocationsAtTime[time];
            foreach (Pair<string> nameAndLocation in namesAndLocations)
            {
                string agentName = nameAndLocation.First;
                string location = nameAndLocation.Second;
                if (dict.ContainsKey(location))
                {
                    dict[location].Add(agentName);
                }
                else
                {
                    dict[location] = new List<string> {agentName};
                }
            }
        }
        else
        {
            Debug.LogError("Could not find " + time + " in ink variables");
        }
        return dict;
    }

    public Dictionary<string, List<String>> GetObjectives()
    {
        return _objectives;
    }

    public string GetInkVariable(string varName)
    {
        string variableValue = "Error";

        if (_inkVariables.ContainsKey(varName))
        {
            variableValue = _inkVariables[varName];
        }
        else
        {
            Debug.LogError("Could not find " + varName + " in ink variables");
        }
        return variableValue;
    }
}