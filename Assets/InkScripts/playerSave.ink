INCLUDE LocationKeer.ink
INCLUDE LocationMR808.ink
INCLUDE DoorStatus.ink

//info updated all the time
VAR CptKeer_profile = "She's the captain of the ship."

//Objectives are printed every time this variable changes under the previous objective printed,
//so that in the end we have a list of all the objecives that we lived trough while playing.
//So, when script start, Objective should be printed right away, when there's that condition at line 28, where the value is changed, it should print the variable again under the thing that was already printed, giving this:
//Find out what happened to you.
//Find out who was at the afterparty.
VAR Objectives = "Find out what happened to you."

//this is so that the conversation hub works
VAR current_entry_knot_keer = 0
VAR current_entry_knot_MR808 = 0 
//keys unlocked or edited during story.
//Can be edited during dialogue (with the ink scripts of agents) 
//or by scanning objects.
VAR AfterParty_key = 0
VAR victors_death = 0
VAR victor_key = 0
VAR VickyPat_item_key = 0
VAR VickyPat_key = 0

VAR door_problem_key = 0
VAR people_were_vented = 0
VAR MR808_routine_key = 0
VAR 3Dprinted_MR808_key = 0
VAR off_line_key = 0

//met characters
VAR met_Laporte = 0
VAR met_CptKeer = 0
VAR met_MR808 = 0
VAR met_MR809 = 0

//agent names
VAR Keer = "Keer"
VAR MR808 = "MR808"
VAR Airlock_Door = "Airlock_Door"

EXTERNAL add_objective(objectiveName, objectiveText)
EXTERNAL update_objective(objectiveName, objectiveText)
EXTERNAL update_location_status(agentName, agentLocationStatus, timeRange)

//{ AfterParty_key > 0 }
//  ~ Objectives = "Find out who was at the afterparty."
//  ~ CptKeer_profile = "She's quite the party-goer... but still the Captain."
  
 