INCLUDE playerSave.ink

//This makes entry from hub or not
{ 
- current_entry_knot_keer == 0: 
	~ met_CptKeer = 1	
	->first_encounter
- else:
	->start_from_hub 
}

=== first_encounter ===
~ current_entry_knot_keer = 1
{add_objective("Welcome", "Welcome to Whodunnit")}
{update_location_status(Keer, "Airlock", "01:00-02:30")}
{update_location_status(MR808, "Somewhere in the Abyss", "06:30")}
P: Good morning Captain
C: There you are, Padma, I was starting to wonder where you were. You're usually the soul of the party, bhuhuhu…
    * P: That seems like a peculiar[ laugh. Are you in need of medical assistance?]
    * P: I am not programmed[ to understand sarcasm.]
- <>-
C: ...and what a party it was! Mrs. Bonnefá had the most wondrous story that had everyone literally rolling on the ground.
{update_objective("Welcome", "You met the Captain, he smells quite strange...")}
-> END
-> party

=== party ===
    * [I'm not familiar with the party you're talking about.]
        Cpt. Keer: Why, I'm talking about Abenzio 452nd birth-quarter, of course. He treated us to the most wonderous meal. And of course he did, with his birthday only one quarter away, you'd expect none the less. 
		-> party
    * [What was Mrs. Bonnefá story?]
        Cpt. Keer: I could not begin to remember. I had quite the eventful night, you see. 
		-> party
    * [Why are you in a maintenance airlock, Captain?]
        -> airlock
    
=== airlock ===

C: Oh, don't worry about that, will you? It's just a little get together.
    * It's not safe to be in an airlock, Captain. I have to politely ask you to leave.
        C: Oh, buh hu. We are here all the time! Nothing bad ever happened. Only today, I've been here since 3 AM, and I have no intention of leaving, obviously.
		~ AfterParty_key = 1
		~ KeerLocation_at_0300 = "Airlock"
		~ KeerLocation_at_0310 = "Airlock"
		~ KeerLocation_at_0320 = "Airlock"
		~ KeerLocation_at_0330 = "Airlock"
		~ KeerLocation_at_0340 = "Airlock"
		~ KeerLocation_at_0350 = "Airlock"
		~ KeerLocation_at_0400 = "Airlock"
		~ KeerLocation_at_0410 = "Airlock"
		~ KeerLocation_at_0420 = "Airlock"
		~ KeerLocation_at_0430 = "Airlock"
		~ KeerLocation_at_0440 = "Airlock"
		~ KeerLocation_at_0450 = "Airlock"
		~ KeerLocation_at_0500 = "Airlock"
		~ KeerLocation_at_0510 = "Airlock"
		~ KeerLocation_at_0520 = "Airlock"
		~ KeerLocation_at_0530 = "Airlock"
		~ KeerLocation_at_0540 = "Airlock"
		~ KeerLocation_at_0550 = "Airlock"
		~ KeerLocation_at_0600 = "Airlock"
		~ KeerLocation_at_0610 = "Airlock"
		~ KeerLocation_at_0620 = "Airlock"
		~ KeerLocation_at_0630 = "Airlock"
		~ KeerLocation_at_0640 = "Airlock"
		~ KeerLocation_at_0650 = "Airlock"
            * * { victors_death>0 }P: I have recently discovered a casualty that took place in this room.
                C: Oh. Well, I'm sure it was his fault.
            * * Who else was here, Captain?
                C: A -> moltitude
    * You seem to be alone, Captain.
            C: Now I am, but a-> moltitude
=== moltitude ===
multitude of people were here! Really only an exclusive selection of the most promising souls on the ship.
    * P: Might I ask you to refrain from tinkering with the atmosphere regulators? 
    * P: I record a health hazard in the composition of this room's atmosphere.
    - C: Don't you worry about that, I. I will definitely not touch the regulators, bhuhuhu...
    * Thank you for your cooperation.
~ current_entry_knot_keer = 1
	-> hub
    
=== start_from_hub ===
Padma: {~Good morning Captain.|How do you do, Captain?|Nice seeing you again, Cpt. Keer.|Might I bother you with some questions, Captain?}
-> hub
=== hub ===
C: Speak freely, my I.
-> hub_in_conv
=== hub_in_conv ===
    * I seem to have been out of order for the night. Do you know what might have happened?
        C: Oh, don't say willywags! You are not designed to be out of order!
        -> out_of_order
    * {victor_key} What do you know of Victor Laporte?
		-> info_on_victor
    * {VickyPat_item_key} I found these pants there, do they remind you of something?
		-> info_on_lovers_item
    * {VickyPat_key} What do you know of VickyPat?
		-> info_on_lover
	* ->exit
=== out_of_order ===
    
    *Yet it seems I was, I'm afraid.
    C: Anyhow, 
    * Did anything out of the ordinary that happened tonight?
    C: 
-<> I wouldn't know. Only thing of interest was our party. But I would hardly call that out of the ordinary.
-> hub_in_conv
=== info_on_victor ===

C: Is he a guest? I know I should, but I really can't remember everybody on this ship. Some people are sooo squeamish...
C: Chances are he was at the party yesterday. All of ICMS Padma was there!
-> hub_in_conv

=== info_on_lovers_item ===

C: I know those pants! VickyPat was wearing them yesterday. How fabulous she was. All eyes were on her pants... or on her butt... or on the movie that was playing on her butt.
~ VickyPat_key = 1
-> hub_in_conv

=== info_on_lover ===

C: Ain't she a darling? Everybody says she deserves my place as the captain. Now, I wouldn't know about that, but she is a darling for sure.
    -> info_on_lover_answers
    === info_on_lover_answers ===
    * (lover_at_party)Was she at the party?
        C: Of course she was! She wouldn't miss it for the world, the little darling.
            * * P: Do you know where she might be now?
                C: Aren't you supposed to know what everybody is doing at all times? Well, I wouldn't know. Last time I saw her she was surreptitiously leaving with some guy. I couldn't very well follow her.
                    * * * Can you tell me anything about that man?
                    * * * Was that man Victor Laporte?
                - - - I wouldn't know. It's not like I was keeping tags on VickyPat all night! 
                -> lover_at_party
            * * Why do you say that?
                C: Well, she just likes to party, I guess. Don't we all?
                -> info_on_lover_answers
                
    * Do you think she deserves to be Captain?
        Now, don't get any ideas. You don't have any say on who's the captain.
        -> info_on_lover_answers
    * -> hub_in_conv
=== exit ===   
* P: Thanks for your cooperation. I'll keep you posted.
    C: Do do that, Padma, my dear.
	-> END