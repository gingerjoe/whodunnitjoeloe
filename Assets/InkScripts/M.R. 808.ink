INCLUDE playerSave.ink

//This makes entry from hub or not
{ current_entry_knot_MR808 == 0: 
    ->first_encounter
- else:
    ->start_from_hub 
}

-> current_entry_knot_MR808

=== first_encounter ===
~ met_MR808 = 1
P: What seems to be the malfunction, M.R. 808?
C: Unable to say. I have been attempting to conduct regulatory airlock diagnostic, but it does not appear to be succesful.
- ~ current_entry_knot_MR808 = 1
 -> hub

=== start_from_hub ===
I require exchange of data, M.R. 808.
-> hub
=== hub ===
* {door_problem_key && not job && not airlock_after_party}P: If my records are correct, the door into space should not be open. Explain.
    C: That is correct. 
-> door
* P: What are you designed duties? 
-> job
* {AfterParty_key} P: Are you aware of a gathering of humans happening taking place in this airlock?
-> airlock_after_party

//Conditional choices to make sure all needed informations are visited 
* {door_problem_key && job || airlock_after_party}P: What is the problem with the door?
    C:
-> door
* {door && not padma_request_topic}P: When did you submit your Padma request?
-> padma_request_topic
* {door && padma_request_topic && people_were_vented == 0}P: I have more questions relating the door malfunction.
    ->tech_of_door

//Exit choice
* P: Resume your designeted duties. [(Exit)]
C: I will.
-> END

=== door ===
<> I have attempted to repair the malfunction, being unsuccesful, I proceded to request assistance from Padma AI unit.
*P: When did you request assistance?
    -> padma_request_topic
*P: Have you tried closing it and opening it again?
*P: How have you attempted to solve the problem?
- C: There didn't seem to be any structural problem, so I proceeded to use manual controls to attempt to restate it to PM. And it worked, which was unexpected, because malfunctioning of lP controls seemed to be the most likely explanation for the FlP.
-> tech_of_door
=== tech_of_door ===
* ->hub
*P: I am not versed in the tecnicalities on how the airlock door works, could you explain it in simpler terms?
    C: I have closed it and opened it again.
    * *Understood.
    * *Thanks for the clarity.
    - - ->tech_of_door
*P: What structural problems did you check for?
C: All the problems related to the correct functioning of the door.
-> tech_of_door
*P: How are the manual controls operated?
C: By operating the appropriate switch.
-> tech_of_door
*P: What does PM mean in this context?
C: Pressurized Mode.
-> tech_of_door
*P: What does lP means?
C: It stands for Laporte. It's both the correct name of this brand of doors and the name of the company producing it.
-> tech_of_door
*P: What does FlP means?
C: It is a kind of malfuction in wich the exterior door opens even if unprotected human beings forms are present in the airlock. It also stands for 'fermé la porte' which is a humorous play on word refering both to the name of the door and the fact that you would expect such a life form to shout 'fermé la porte', where him or her a French speaker.
~ people_were_vented = 1
    * * P: What element of the play of words makes it humorous?
        C: I am not designed to understand humor.
        -> tech_of_door
    * * P: Were there unprotected people in the airlock when the door was opened?
    C: That is what led me to suspect there might be some malfuction in the door. Two naked people flew out of the airlock when the door opened to let me in.
        -> who_were_vented_people
// SUPER IMPORTANT FOR ROMAN OR TIM: all the stuff from here on, until it gets back to tech_of_door, would not be experienced at all if the player choses the comment on the play of words, instead of info on the people. Is that too harsh? Is that a choice in case player want to know more about it or not? Is it misleading (the play of words is much more fun to chose than the boring: "tell me what happened". The game still works because there are no VAR in this bit, but it's info and fun content.
         = who_were_vented_people
        * * *P: Didn't you judge people sucked out in space to be the most relevan detail of the situation?
            C: That is only the consequence of the problem. I am designed to investigate only the cause and solution of the problem.
              ->who_were_vented_people
        * * *P: Why were they naked?
            C: I am not designed to be aware of humans' uses and costumes. In case such information were to be required, I am designed to ask you for it.
            ->who_were_vented_people
        * * *P: Where did they go?
            C: Unless you have information on humans not yet included in my firmware, I do not believe they would have been able to survive naked in space. I do not know why might they have exited.
            ->who_were_vented_people
        * * *P: Who were they?
            C: The manifest database of the ship is not included in my firmware.
            -> who_were_vented_people
            * * * P: How did they look like?
                    C: Naked, caucasian. Also full of water, which seems couterproductive when flying into space, my safety instructions stress. But they flew off much too fast for my sensors to recognize any facial feature.
                  -> back_to_door_discussion
= back_to_door_discussion
*P: I have more questions about the door.
*P: I am testing a new human social construct. Could you act as if it's ordinary for me not to remember we were talking about the door?
    C: Yes.
    * * P: Thank you. We got sidetracked. What were we talking about again?
    * * P: We got sidetracked. What were we talking about again?
    - -C: About the door malfunction.
- ->tech_of_door

=== padma_request_topic ===
B: I submitted the first request at 02:03.
~ MR808Location_at_0200 = "airlock"
	* P: The first?
	* P: Did you submit more than one request?
	- C: Yes. I proceeded to submit a new request after every attempt at fixing the door.
	* P: How many times did you try to fix it?
	    C: 
	* P: Why didn't you try all you could to fix the door, before calling me?
	    C: I did.
	- <> I went through the regulatory procedure 16 times, before my Padma request was answered.
	-> regulatory_procedure
	= regulatory_procedure
	 * P: How long does it take you to go through the regulatory procedure?
	    C: Ten minutes.
	    ~ MR808Location_at_0150 = "airlock"
        ~ MR808Location_at_0200 = "airlock"
        ~ MR808Location_at_0210 = "airlock"
        ~ MR808Location_at_0220 = "airlock"
        ~ MR808Location_at_0230 = "airlock"
        ~ MR808Location_at_0240 = "airlock"
        ~ MR808Location_at_0250 = "airlock"
        ~ MR808Location_at_0300 = "airlock"
        ~ MR808Location_at_0310 = "airlock"
        ~ MR808Location_at_0320 = "airlock"
        ~ MR808Location_at_0330 = "airlock"
        ~ MR808Location_at_0340 = "airlock"
        ~ MR808Location_at_0350 = "airlock"
        ~ MR808Location_at_0400 = "airlock"
        ~ MR808Location_at_0410 = "airlock"
        ~ MR808Location_at_0420 = "airlock"
        ~ MR808Location_at_0430 = "airlock"
        ~ MR808Location_at_0440 = "airlock"
        ~ MR808Location_at_0450 = "airlock"
        ~ MR808Location_at_0500 = "airlock"
        ~ MR808Location_at_0510 = "airlock"
        ~ MR808Location_at_0520 = "airlock"
        ~ MR808Location_at_0530 = "airlock"
        ~ MR808Location_at_0540 = "airlock"
        ~ MR808Location_at_0550 = "airlock"
        ~ MR808Location_at_0600 = "airlock"
        ~ MR808Location_at_0610 = "airlock"
        ~ MR808Location_at_0620 = "airlock"
        ~ MR808Location_at_0630 = "airlock"
        ~ MR808Location_at_0640 = "airlock"
        ~ MR808Location_at_0650 = "airlock"
	    -> regulatory_procedure
	 * P: What was the regulatory procedure in this instance?
	    C: To close it and open it again.
        ~ AirlockDoorStatus_at_0150 = "closed"
        ~ AirlockDoorStatus_at_0200 = "open"
        ~ AirlockDoorStatus_at_0210 = "closed"
        ~ AirlockDoorStatus_at_0220 = "open"
        ~ AirlockDoorStatus_at_0230 = "closed"
        ~ AirlockDoorStatus_at_0240 = "open"
        ~ AirlockDoorStatus_at_0250 = "closed"
        ~ AirlockDoorStatus_at_0300 = "open"
        ~ AirlockDoorStatus_at_0310 = "closed"
        ~ AirlockDoorStatus_at_0320 = "open"
        ~ AirlockDoorStatus_at_0330 = "closed"
        ~ AirlockDoorStatus_at_0340 = "open"
        ~ AirlockDoorStatus_at_0350 = "closed"
        ~ AirlockDoorStatus_at_0400 = "open"
        ~ AirlockDoorStatus_at_0410 = "closed"
        ~ AirlockDoorStatus_at_0420 = "open"
        ~ AirlockDoorStatus_at_0430 = "closed"
        ~ AirlockDoorStatus_at_0440 = "open"
        ~ AirlockDoorStatus_at_0450 = "closed"
        ~ AirlockDoorStatus_at_0500 = "open"
        ~ AirlockDoorStatus_at_0510 = "closed"
        ~ AirlockDoorStatus_at_0520 = "open"
        ~ AirlockDoorStatus_at_0530 = "closed"
        ~ AirlockDoorStatus_at_0540 = "open"
        ~ AirlockDoorStatus_at_0550 = "closed"
        ~ AirlockDoorStatus_at_0600 = "open"
        ~ AirlockDoorStatus_at_0610 = "closed"
        ~ AirlockDoorStatus_at_0620 = "open"
        ~ AirlockDoorStatus_at_0630 = "closed"
        ~ AirlockDoorStatus_at_0640 = "open"
        ~ AirlockDoorStatus_at_0650 = "closed"
	 -> regulatory_procedure
    * ->hub

=== job ===
~ MR808_routine_key = 1
~ 3Dprinted_MR808_key = 1
C: I have been printed to perform the dayly maintenance check-up of the outer hull.
    ->job_answers
    = job_answers
	*P: When have you been printed?
	    C: At 00:40.
	    ~ MR808Location_at_0040 = "outer hull"
	    -> job_answers
	*P: Did you encounter any problems during the check-up?
	    C: There seemed to be an unaccounted for change at the sail stand 34, but upon closer inspection, I concluded it was only a reflection artifact.
	        -> job_answers
	*P: Why are you in this airlock, then?
	    C: This is the closest airlock to the disassembly station. Having completed my duties, I was on my way to be recycled.
	        * * { MR808Location_at_0040 == "outer hull"}P: Why are you still here?
	        * * { MR808Location_at_0040 == "Unknown"}P: Do not let me hold you, then.
	        - -C: I am unable to proceed because I found a malfunction I have the responsibility to fix.
	            * * *P: Did you not say there was nothing out of the ordinary during your check-up?
	                 C: I did, but
	            * * *P: What is the malfunction?
	                 C:
	            - - -<> I found a malfunction with the door of this airlock.
	            ~ door_problem_key = 1
	- ->hub

//this was here because at some point I remembered a detail about the vented people the player should know that is not covered in the big rant in === door ===. but then I forgot again, so yeah, I'll leave it here, in case I remember next time I go trough this.
//=== vented_people_topic ===
//d
//-> hub

=== airlock_after_party ===
B: Yes, a number of people have been here preventing me to appropriately try to fix the door.
*{door}P: That is, opening and closing it again.
    C: That is correct.
*P: How were they interrupting your work?
    C:
-<> If I had opened the door with them inside, I would have put their lifes in danger, which prevented me to perform my duty appropriately.
    ~ door_problem_key = 1
->hub

-> END