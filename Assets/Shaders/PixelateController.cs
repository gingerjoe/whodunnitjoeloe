﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class PixelateController : MonoBehaviour {

    private const float DefaultPixelSize = 1.0f;
    public float MaxPixelSize = 5.0f;
    public float TransitionTime = 5.0f;

    private string _pixelateShaderLocation = "Custom/PixelationShader";
    private string _pixelateShaderPixelSizeName = "_PixelSize";
    private Material _pixelateShaderMaterial;

    private int _pixelateDirection = 1;
    private float _currentTime = 1.0f;

    void Start()
    {
        Renderer renderer = gameObject.GetComponentInChildren<Renderer>();
        _pixelateShaderMaterial = renderer.materials.Single(m => m.shader.name == _pixelateShaderLocation);

        Verify();
        SetPixelation(MaxPixelSize);
    }

    public void SetPixelation(float size)
    {
        _pixelateShaderMaterial.SetFloat(_pixelateShaderPixelSizeName, size);
    }

    public void AddPixelation()
    {
        BeginPixelation(1);
    }

    public void RemovePixelation()
    {
        BeginPixelation(-1);
    }

    public void BeginPixelation(int direction)
    {
        _pixelateDirection = direction;
    }

    void Update()
    {
        _currentTime += (Time.deltaTime / TransitionTime) * _pixelateDirection;
        _currentTime = Mathf.Clamp01(_currentTime);

        float newPixelSize = Mathf.Lerp(DefaultPixelSize, MaxPixelSize, _currentTime);
        _pixelateShaderMaterial.SetFloat(_pixelateShaderPixelSizeName, newPixelSize);
    }

    private void Verify()
    {
        if (_pixelateShaderMaterial == null)
        {
            throw new MissingComponentException(_pixelateShaderLocation + " is not attached to any material associated with the renderer");
        }
    }
}
