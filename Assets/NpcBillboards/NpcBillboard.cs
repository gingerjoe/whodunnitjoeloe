﻿using UnityEngine;
using System.Collections;

public class NpcBillboard : MonoBehaviour {

    private Transform cam;

    void Start () {
        cam = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

	void Update () {
        transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);
    }

    protected Transform Cam
    {
        get { return cam; }
    }

}
