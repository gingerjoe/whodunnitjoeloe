﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;

public class TimelineScript : MonoBehaviour
{
    public InkManager InkManager;
    public GameObject Clock;
    public int StartHour = 0;
    public int StartMinute = 0;
    public int StepSize = 10;

    private Slider _timeSlider;
    private Text _clockText;
    private GameObject[] _rooms;
    private GameObject[] _agents;
    private Camera _camera;

    void Start()
    {
        _timeSlider = gameObject.GetComponentInChildren<Slider>();
        _clockText = Clock.GetComponent<Text>();
        _rooms = GameObject.FindGameObjectsWithTag("Room");
        _agents = GameObject.FindGameObjectsWithTag("Agent");
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        foreach (GameObject agent in _agents)
        {
            agent.SetActive(false);
        }

        ChangeTime();
    }

    public void ChangeTime()
    {
        int val = (int)_timeSlider.value;
        int temptime = val * StepSize;

        int hours = StartHour + (temptime / 60);
        string hoursString = hours.ToString();
        int minutes = StartMinute + (temptime % 60);
        string minutesString = minutes.ToString();

        if (hours < 10)
        {
            hoursString = string.Concat("0", hoursString);
        }
        if (minutes < 10)
        {
            minutesString = string.Concat("0", minutesString);
        }

        string clockTime = hoursString + ":" + minutesString;

        _clockText.text = clockTime;
        PopulateRooms(InkManager.GetAgentsAtLocationFromTime(clockTime), clockTime);
    }

    public void SetTime(int x)
    {
        _timeSlider.value = x;
    }

    public void PopulateRooms(Dictionary<string, List<string>> locationDict, String time)
    {
        foreach (GameObject agent in _agents)
        {
            bool spawned = false;
            foreach (GameObject room in _rooms)
            {
                string roomName = room.name;
                if (locationDict.ContainsKey(roomName) && locationDict[roomName].Contains(agent.name))
                {
                        Debug.Log("Placing agent [" + agent.name + "] into room [" + roomName + "] at time [" + time +
                                  "].");
                        //Currently placing agents in center of rooms (on top of eachother) add a random displacement within bounds
                        Vector3 roomPos = _camera.WorldToScreenPoint(room.transform.localPosition);
                        agent.SetActive(true);
                        agent.transform.position = roomPos;
                        spawned = true;
                        break;
                }
            }

            if (!spawned)
            {
                agent.SetActive(false);
            }
        }
    }
}