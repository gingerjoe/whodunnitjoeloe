﻿using UnityEngine;
using System.Collections;

public class RoomScript : MonoBehaviour
{
    public Bounds area;

    void Start()
    {
        area = gameObject.GetComponentInChildren<BoxCollider>().bounds;
    }
}