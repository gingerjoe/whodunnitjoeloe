﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayPersonInfo : MonoBehaviour {

    public InkManager inkManager;
    public GameObject profile;
    public Text profileTitle;
    public Text profileDescription;
    public Image profileImage;

    public Sprite imageToDisplay;

    private Text _name;

    Ray ray;
    RaycastHit hit;


    void Start()
    {
        //inkManager = GameObject.Find("InkManager").GetComponent<InkManager>();
        _name = gameObject.GetComponentInChildren<Text>();
        HidePersonInfo();
    }

    //void Update()
    //{
    //    Originally had the profile display when hovering over the timeline objects.May still want to reuse this ?

    //  ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //    if (Physics.Raycast(ray, out hit) && hit.collider.tag == "TimelineObject")
    //    {
    //        ShowPersonInfo();
    //    }
    //    else
    //    {
    //        HidePersonInfo();
    //    }
    //}

    public void ShowPersonInfo()
    {
        profile.SetActive(true);
        profileTitle.text = _name.text;
        profileDescription.text = inkManager.GetInkVariable(gameObject.name + "_profile");

        if (imageToDisplay)
        {
            profileImage.sprite = imageToDisplay;
        }
    }

    public void HidePersonInfo()
    {
        profile.SetActive(false);
    }

}
