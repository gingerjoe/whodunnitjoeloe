﻿﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ObjectivesScript : MonoBehaviour
{

    public InkManager inkManager;
    public Text objectiveText;

    private Dictionary<string, List<string>> objectivesDictionary;

    void Start()
    {
        objectivesDictionary = inkManager.GetObjectives();

        UpdateObjectivePanel();
    }

    public void UpdateObjectivePanel()
    {
        objectiveText.text = "";
        foreach (string objectiveName in objectivesDictionary.Keys)
        {
            List<string> objectives = objectivesDictionary[objectiveName];

            objectiveText.text += "\n";

            if (objectives.Contains("DONE"))
            {
                objectiveText.text += string.Concat("<b><color=green>", objectiveName, "</color></b>");
            }
            else
            {
                objectiveText.text += string.Concat("<b><color=white>", objectiveName, "</color></b>");

                for (int i = 0; i < objectives.Count; i++)
                {
                    objectiveText.text += "\n";

                    string obj = objectives[i].ToString();
                    if (i != objectives.Count - 1)
                    {
                        objectiveText.text += string.Concat("<i>", "   - ", obj, "<color=green>-COMPLETED-</color></i>");
                    }
                    else
                    {
                        objectiveText.text += string.Concat("   - ", obj);
                    }
                }
            }
        }
    }
}