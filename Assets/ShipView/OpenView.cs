﻿using UnityEngine;
using System.Collections;

public class OpenView : MonoBehaviour
{
    public GameObject panel;

    private RectTransform _panelSize;
    private bool _moving;
    private float _boundsX;
    private float _boundsY;
    private Vector3 _bounds;
    private Vector3 _originalPos;
    private Vector3 _outVect;

    private float _currentTime = 0;
    private int _direction = 1;

    void Start()
    {
        _panelSize = panel.GetComponent<RectTransform>();
        _moving = false;

        _originalPos = panel.transform.position;
        _outVect = gameObject.transform.position - panel.transform.position;
        _outVect.Normalize();

        if (_originalPos.x < 0)
        {
            _boundsX = Mathf.Abs(_panelSize.rect.width / 2);
            _boundsY = Screen.height / 2;
        }
        else if (_originalPos.x > Screen.width)
        {
            _boundsX = Mathf.Abs(Screen.width - _panelSize.rect.width / 2);
            _boundsY = Screen.height / 2;
        }
        else if (_originalPos.y > Screen.height)
        {
            _boundsX = Screen.width / 2;
            _boundsY = Mathf.Abs(Screen.height - _panelSize.rect.height / 2);
        }

        _bounds = new Vector3(_boundsX, _boundsY);
        Debug.Log(gameObject.name + " Bounds: " + _bounds);
    }

    private bool _opening = false;

    void Update()
    {
        if (_moving)
        {
            _currentTime += (Time.deltaTime / Global.Instance.PanelSlideAnimationTime) * _direction;
            _currentTime = Mathf.Clamp01(_currentTime);

            float newX = Mathf.Lerp(_originalPos.x, _boundsX, _currentTime);
            float newY = Mathf.Lerp(_originalPos.y, _boundsY, _currentTime);
            //Debug.Log("x:" + newX + ", y:" + newY + ", ct:" + _currentTime);

            Vector3 newPos = new Vector3(newX, newY);

            panel.transform.Translate(newPos - panel.transform.position);

            if (_currentTime <= 0 || _currentTime >= 1)
            {
                _moving =  !_moving;
            }
        }
    }

    public void Transition()
    {
        if (_opening)
        {
            HideView();
        }
        else
        {
            ShowView();
        }
    }

    public void ShowView()
    {
        Debug.Log("Showing panel: " + this.name);
        BeginLerp(+1);
        _moving = true;
        _opening = true;
    }

    public void HideView()
    {
        Debug.Log("Hiding panel: " + this.name);
        BeginLerp(-1);
        _moving = true;
        _opening = false;
    }

    public void BeginLerp(int direction)
    {
        _direction = direction;
    }
}